<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();

// Route::get('/', function () {
//     return view('welcome');
// });

/**
 * dashboard
 */
Route::group(['prefix' => 'dashboard'], function () {
	Route::get('/', 'web\DashboardController@showBoard');		// 顯示看板畫面
	Route::get('{profile}', 'web\DashboardController@index');	// 製令看板
	Route::get('profile/{profile}/date/{date}', 'web\DashboardController@getAccumulateData');	// 績效看板
});

/**
 * admin
 */
Route::get('login', function () {
	return view('auth.login');
});

Route::get('register', function () {
	return view('auth.register');
});
Route::post('register', 'web\UserController@store');
Route::post('login', 'web\AuthController@auth');
Route::post('logout', 'web\AuthController@logout');

Route::group(['middleware' => 'auth.web'], function() {
	
	Route::get('/', function () {
		return view('home');
	});
	
	Route::group(['prefix' => 'mes'], function () {
		Route::get('rfid', 'web\MesController@rfidPage');					// RFID資料數據畫面
		Route::get('recording', 'web\MesController@mappingPage');				// 錄入模組畫面
		Route::get('mo/rfid', 'web\MesController@moRfidPage');				// 製令RFID報工數據畫面

		Route::post('rfid/data', 'web\MesController@getRfidData');				// RFID數據
		Route::post('rfid/history', 'web\MesController@getRfidHistory'); 		// RFID歷史紀錄

		Route::post('mo/rfid/data', 'web\MesController@getMoRfid');			// 製令RFID報工數據
		Route::post('mo/rfid/history', 'web\MesController@getMoRfidHistory');	// 製令RFID歷史紀錄
	});

	Route::group(['prefix' => 'setup'], function () {
		Route::group(['prefix' => 'unfinished'], function () {
			Route::get('/', 'web\SetupController@showNgPage');
			Route::post('store', 'web\SetupController@storeUnfinished');
			Route::put('update/{id}', 'web\SetupController@updateUnfinished');
			Route::delete('destroy/{id}', 'web\SetupController@destroyUnfinished');
		});
		
		Route::group(['prefix' => 'exception'], function () {
			Route::get('/', 'web\SetupController@showExceptionPage');
			Route::post('store', 'web\SetupController@storeException');
			Route::put('update/{id}', 'web\SetupController@updateException');
			Route::delete('destroy/{id}', 'web\SetupController@destroyException');
		});

		Route::group(['prefix' => 'suspend'], function () {
			Route::get('/', 'web\SetupController@showSuspendPage');
			Route::post('store', 'web\SetupController@storeSuspend');
			Route::put('update/{id}', 'web\SetupController@updateSuspend');
			Route::delete('destroy/{id}', 'web\SetupController@destroySuspend');
		});
	});

	Route::group(['prefix' => 'device'], function () {
		Route::group(['prefix' => 'tablet'], function () {
			Route::get('/', 'web\DeviceController@showTablets')->name('tablet.index');
			Route::post('store', 'web\DeviceController@storeTablet');
			Route::put('update/{id}', 'web\DeviceController@updateTablet');
			Route::delete('destroy/{id}', 'web\DeviceController@destroyTablet');
		});

		Route::group(['prefix' => 'board'], function () {
			Route::get('/', 'web\DeviceController@showBoards')->name('board.index');
			Route::post('store', 'web\DeviceController@storeBoard');
			Route::put('update/{id}', 'web\DeviceController@updateBoard');
			Route::delete('destroy/{id}', 'web\DeviceController@destroyBoard');
		});

		Route::group(['prefix' => 'print'], function () {
			Route::get('/', 'web\DeviceController@showPrints')->name('print.index');
			Route::post('store', 'web\DeviceController@storePrint');
			Route::put('update/{id}', 'web\DeviceController@updatePrint');
			Route::delete('destroy/{id}', 'web\DeviceController@destroyPrint');
		});

		Route::group(['prefix' => 'antenna'], function () {
			Route::get('/', 'web\DeviceController@showAntennas')->name('antenna.index');
			Route::post('store', 'web\DeviceController@storeAntenna');
			Route::put('update/{id}', 'web\DeviceController@updateAntenna');
			Route::delete('destroy/{id}', 'web\DeviceController@destroyAntenna');
		});
	});

	Route::group(['prefix' => 'user'], function () {
		Route::post('update', 'web\UserController@update');
		Route::get('edit', 'web\UserController@edit');
	});
});
