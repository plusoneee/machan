var select = document.getElementById('selectProfile');
var profile = null;
var name = '請選擇看板線群';
var nowTime_1 = document.getElementById('now_time_1');
var nowTime_2 = document.getElementById('now_time_2');
var nowTime_3 = document.getElementById('now_time_3');
var col_1 = document.getElementById('col_1');
var col_2 = document.getElementById('col_2');
var col_3 = document.getElementById('col_3');
var rowBoard = document.getElementById('rowBoard');
var now = null;
var hour, minute, second;

changeProfile = () => {
  profile = select.options[select.selectedIndex].value;
  name = select.options[select.selectedIndex].text;
}

setInterval(() => {
  now = new Date();
  hour = now.getHours() < 10 ? '0' + now.getHours() : now.getHours();
  minute = now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes();
  second = now.getSeconds() < 10 ? '0' + now.getSeconds() : now.getSeconds();
  document.getElementById('now_time_1').innerHTML = hour + ':' + minute + ':' + second;
  document.getElementById('now_time_2').innerHTML = hour + ':' + minute + ':' + second;
  document.getElementById('now_time_3').innerHTML = hour + ':' + minute + ':' + second;
  document.getElementById('nowTime').innerHTML = hour + ':' + minute + ':' + second;
  document.getElementById('profile').innerHTML = now.getFullYear() + '/' + ((now.getMonth() + 1) < 10 ? '0' + (now.getMonth() + 1) : (now.getMonth() + 1)) + '/' + (now.getDate() < 10 ? '0' + (now.getDate()) : now.getDate()) + '&nbsp;' + name;
}, 1000);

pullAccuData = () => {
  now = new Date();
  var date = now.getFullYear() + '-' + ((now.getMonth() + 1) < 10 ? '0' + (now.getMonth() + 1) : (now.getMonth() + 1)) + '-' + (now.getDate() < 10 ? '0' + (now.getDate()) : now.getDate());
  $.ajax({
    url: '/dashboard/profile/' + profile + '/date/' + date,
    type: 'GET',
    dataType: 'JSON',
    success: (data) => {
      document.getElementById('accumulator_start').innerHTML = data.accuInfo.accuStart;
      document.getElementById('accumulator_fin').innerHTML = data.accuInfo.accuEnd;
      document.getElementById('day_accu_pass_rate').innerHTML = '當日累計一次通過率：' + data.accuInfo.accuPassRate;
      document.getElementById('accu_nf_qty').innerHTML = '累計尾數：' + data.accuInfo.accuNf;
      document.getElementById('finCount').innerHTML = '已完成製令數：' + data.accuInfo.finCount;
      document.getElementById('accuRealQty').innerHTML = data.accuInfo.accuRealQty;
      document.getElementById('accu_change_time').innerHTML = '累計換線時間：' + data.accuInfo.accuChangeTime;
      document.getElementById('mo_count').innerHTML = '線別常駐指令數：' + data.accuInfo.moCount;
      document.getElementById('target_qty').innerHTML = data.accuInfo.accuTargetQty;
      document.getElementById('qtyRate').innerHTML = data.accuInfo.qtyRate + '%';
      changeBackground(data.accuInfo.first_condition, data.accuInfo.second_condition, data.accuInfo.qtyRate);
    }
  });
}

pullData = () => {
  $.ajax({
    url: '/dashboard/' + profile,
    type: 'GET',
    dataType: 'JSON',
    success: (data) => {
      if (profile == '' || data.rfidInfo[0].length == 0) {
        rowBoard.style.display = 'none';
        document.getElementById('accumulatorBoard').style.display = '';
        checkWorkStatus(data, name);
        pullAccuData();
      } else {
        document.getElementById('accumulatorBoard').style.display = 'none';
        rowBoard.style.display = '';
        checkWorkStatus(data, name);

        if (data.rfidInfo[0].length == 1) {
          col_1.className = 'col-md-6';
          col_1.style.marginLeft = '25%';
          col_1.style.display = '';
          col_2.style.display = 'none';
          col_3.style.display = 'none';
          data.rfidInfo[0].map((m, key) => {
            if (m.rank == 1) {
              changeDisplay(m.rank, data, key);
            }
          })
        } else if (data.rfidInfo[0].length == 2) {
          col_1.style.display = '';
          col_1.className = 'col-md-6';
          col_1.style.marginLeft = '0px';
          col_2.className = 'col-md-6';
          col_2.style.display = '';
          col_3.style.display = 'none';
          data.rfidInfo[0].map((m, key) => {
            if (m.rank == 1) {
              changeDisplay(m.rank, data, key);
            }

            if (m.rank == 2) {
              changeDisplay(m.rank, data, key);
            }
          })
        } else if (data.rfidInfo[0].length >= 3) {
          col_1.className = 'col-md-4';
          col_1.style.marginLeft = '0px';
          col_2.className = 'col-md-4';
          col_3.className = 'col-md-4';
          col_1.style.display = '';
          col_2.style.display = '';
          col_3.style.display = '';
          data.rfidInfo[0].map((m, key) => {
            if (m.rank == 1) {
              changeDisplay(m.rank, data, key);
            }

            if (m.rank == 2) {
              changeDisplay(m.rank, data, key);
            }

            if (m.rank == 3) {
              changeDisplay(m.rank, data, key);
            }
          })
        }
      }
    }
  });
}

checkWorkStatus = (data, lineName) => {
  if (data.rfidInfo[4] == 1) {
    document.getElementById('workStatus').innerHTML = '請點選上班按鈕';
  } else if (data.rfidInfo[4] == 0) {
    document.getElementById('workStatus').innerHTML = '';
  } 
  if (data.rfidInfo[5] == 0) {
    document.getElementById('workStatus').innerHTML = name + '已下班';
  }
}

// 改變看板顯示資料
changeDisplay = (rank, data, key) => {
  document.getElementById('exp_dif_time_' + rank).innerHTML = data.rfidInfo[0][key].different_time;
  document.getElementById('ord_qty_' + rank).innerHTML = data.rfidInfo[0][key].qty == '' ? '-' : data.rfidInfo[0][key].qty;
  document.getElementById('exp_fin_time_' + rank).innerHTML = data.rfidInfo[0][key].completion_date;
  document.getElementById('start_time_' + rank).innerHTML = data.rfidInfo[0][key].start_time == '' ? '-' : data.rfidInfo[0][key].start_time;
  document.getElementById('fin_time_' + rank).innerHTML = data.rfidInfo[0][key].finish_time == null ? '-' : data.rfidInfo[0][key].finish_time;
  document.getElementById('fin_time_' + rank).style.color = 'red';
  document.getElementById('input_qty_' + rank).innerHTML = data.rfidInfo[0][key].accumulator_start;
  document.getElementById('fin_qty_' + rank).innerHTML = data.rfidInfo[0][key].accumulator_fin;
  document.getElementById('tail_num_' + rank).innerHTML = data.rfidInfo[0][key].nf_qty;
  document.getElementById('ord_client_' + rank).innerHTML = '客戶：' + data.rfidInfo[0][key].customer_name;
  document.getElementById('pro_name_' + rank).innerHTML = '產品名稱：' + data.rfidInfo[0][key].item;
  document.getElementById('show_order_' + rank).innerHTML = '顯示順序：' + rank;
  document.getElementById('show_qty_' + rank).innerHTML = '當線顯示數：' + (data.rfidInfo[0].length);
  document.getElementById('status_' + rank).innerHTML = data.rfidInfo[0][key].finish_time == null ? '報工中' : '完工結案';
  if (data.rfidInfo[0][key].actureHuman != 0) {
    document.getElementById('exp_fin_time_' + rank).style.color = 'red';
  } else {
    document.getElementById('exp_fin_time_' + rank).style.color = 'black';
  }

  if (data.rfidInfo[0][key].mo_id == data.rfidInfo[1]) {
    if (data.rfidInfo[3] == 'start') {
      if (data.rfidInfo[2] == 'B') {
        if (rank == 1) {
            document.getElementById('input_qty_1').style.color = '#00AA00';
            document.getElementById('input_qty_2').style.color = '#000000';
            document.getElementById('input_qty_3').style.color = '#000000';
            finColor();
        }
        if (rank == 2) {
          document.getElementById('input_qty_1').style.color = '#000000';
          document.getElementById('input_qty_2').style.color = '#00AA00';
          document.getElementById('input_qty_3').style.color = '#000000';
          finColor();
        }
        if (rank == 3) {
          document.getElementById('input_qty_1').style.color = '#000000';
          document.getElementById('input_qty_2').style.color = '#000000';
          document.getElementById('input_qty_3').style.color = '#00AA00';
          finColor();
        }
      }
    } else if (data.rfidInfo[3] == 'end') {
      if (data.rfidInfo[2] == 'B') {
        if (rank == 1) {
          inputColor();
          document.getElementById('fin_qty_1').style.color = '#00AA00';
          document.getElementById('fin_qty_2').style.color = '#000000';
          document.getElementById('fin_qty_3').style.color = '#000000';
        }
        if (rank == 2) {
          inputColor();
          document.getElementById('fin_qty_1').style.color = '#000000';
          document.getElementById('fin_qty_2').style.color = '#00AA00';
          document.getElementById('fin_qty_3').style.color = '#000000';
        }
        if (rank == 3) {
          inputColor();
          document.getElementById('fin_qty_1').style.color = '#000000';
          document.getElementById('fin_qty_2').style.color = '#000000';
          document.getElementById('fin_qty_3').style.color = '#00AA00';
        }
      }
    }
  }
}

inputColor = () => {
  document.getElementById('input_qty_1').style.color = '#000000';
  document.getElementById('input_qty_2').style.color = '#000000';
  document.getElementById('input_qty_3').style.color = '#000000';
}

finColor = () => {
  document.getElementById('fin_qty_1').style.color = '#000000';
  document.getElementById('fin_qty_2').style.color = '#000000';
  document.getElementById('fin_qty_3').style.color = '#000000'; 
}

// 切換績效看板背景色
changeBackground = (first, second, rate) => {
  for (let i = 1; i <= 6; i++) {
    if (rate < second && rate >= 0) {
      document.getElementById('accu_' + i).style.background = '#FFA488';
    } else if (rate >= second && rate < first) {
      document.getElementById('accu_' + i).style.background = '#FFFF00';
    } else if (rate >= first) {
      document.getElementById('accu_' + i).style.background = '#66FF66';
    }
  }
}

setInterval(pullData, 3000);