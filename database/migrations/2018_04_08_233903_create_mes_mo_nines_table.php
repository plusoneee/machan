<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoNinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_nines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dashboard_id');
            $table->string('factory_id');
            $table->string('line_id');
            $table->integer('mo_status');
            $table->integer('mo_id');
            $table->string('item');
            $table->integer('qty');
            $table->string('so_id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->integer('group_id');
            $table->integer('group_qty');
            $table->integer('prod_qty');
            $table->integer('fin_qty');
            $table->integer('ng_qty');
            $table->dateTime('start_datetime')->nullable();
            $table->dateTime('finish_datetime')->nullable();
            $table->time('f_completion_time')->nullable();
            $table->string('rest_time');
            $table->double('about_qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_nines');
    }
}
