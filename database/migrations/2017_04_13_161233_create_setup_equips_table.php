<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupEquipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_equips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('person_id');            //員工工號
            $table->string('name');                 //員工姓名
            $table->string('role_id');              //角色
            $table->string('role_name');            //角色名稱
            $table->string('tablet_id');            //設備ID
            $table->string('line_id');
            $table->string('org_id');
            $table->string('routing');
            $table->string('profile');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_equips');
    }
}
