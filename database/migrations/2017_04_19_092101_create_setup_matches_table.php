<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_matches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('profile');
            $table->string('org_id');
            $table->string('routing');
            $table->string('line_id');
            $table->string('antenna_id');
            $table->string('antenna_name');
            $table->Integer('antenna_type');
            $table->Integer('port_no');
            $table->string('port_id');
            $table->string('status_at');
            $table->string('reader_id');
            $table->string('reader_name');
            $table->string('reader_ip');
            $table->string('status_rd');
            $table->string('tablet_id');
            $table->string('ip');
            $table->string('person_id');
            $table->string('name');
            $table->string('status_tb');
            $table->string('b_print_id');
            $table->string('b_print_name');
            $table->string('status_bp');
            $table->string('dashboard_id');
            $table->string('dashboard_name');
            $table->string('status_db');
            $table->string('server');
            $table->string('server_ip');
            $table->string('status_sv');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_matches');
    }
}
