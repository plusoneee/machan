<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbWebsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eb_webs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_id');
            $table->string('org_id');
            $table->string('routing');
            $table->string('type');
            $table->string('line_id');
            $table->string('dashboard_id');
            $table->string('profile');
            $table->Integer('status');
            $table->string('eb_qty');
            $table->string('line_mo_qty');
            $table->string('rank');
            $table->Integer('day_mo_qty');
            $table->Integer('day_accumulator_start');
            $table->Integer('day_accumulator_fin');
            $table->Integer('day_ng');
            $table->Integer('day_qty');
            $table->string('day_first_pass_rate');
            $table->string('day_change_lead_time');
            $table->string('day_suspend_time');
            $table->string('object_equivalents');
            $table->string('accu_fin_equivalents');
            $table->string('capacity_rate');
            $table->string('color');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eb_webs');
    }
}
