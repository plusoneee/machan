<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoHeaderTwosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_header_twos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('factory_id');
            $table->string('line_id');
            $table->string('inserted_mo_id');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->double('delta_t');
            $table->date('insert_start_date')->nullable();
            $table->time('insert_start_time')->nullable();
            $table->date('insert_finish_date')->nullable();
            $table->time('insert_finish_time')->nullable();
            $table->integer('accu_insert_start');
            $table->integer('accu_insert_finish');
            $table->time('cl_start_time')->nullable();
            $table->time('cl_end_time')->nullable();
            $table->string('insert_change_time');
            $table->double('insert_delta_tct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_header_twos');
    }
}
