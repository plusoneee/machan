<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesQcPassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_qc_passes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qc_year');              //區分序號編號
            $table->string('mac_id');
            $table->string('rfid_type');
            $table->string('record_id');
            $table->string('company_id');
            $table->string('factory_id');
            $table->integer('rfid_status');
            $table->date('record_date')->nullable();
            $table->date('erase_date')->nullable();
            $table->date('disable_date')->nullable();
            $table->integer('enable')->default(1);
            $table->string('qc_pass');
            $table->string('qc');
            $table->date('print_date')->nullable();
            $table->time('print_time')->nullable();
            $table->date('force_print_date')->nullable();
            $table->time('force_print_time')->nullable();
            $table->date('reprint_date')->nullable();
            $table->time('reprint_time')->nullable();
            $table->integer('printed');
            $table->integer('force_print');
            $table->integer('reprint');
            $table->string('mo_id');
            $table->string('item');
            $table->integer('qty');
            $table->string('completion_date');
            $table->string('so_id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->string('org_id');
            $table->integer('mo_status');
            $table->string('device_id');
            $table->string('antenna_id');
            $table->string('antenna_type');
            $table->string('port_no');
            $table->string('port_id');
            $table->string('pc');
            $table->string('fcc');
            $table->string('crc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_qc_passes');
    }
}
