<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesOffWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_off_works', function (Blueprint $table) {
            $table->increments('id');
            $table->string('profile');
            $table->string('line_id');
            $table->string('routing');
            $table->date('off_work_date')->nullable();                //線別APP上下班按鈕時間
            $table->time('off_work_time')->nullable();                //線別APP上下班按鈕時間
            $table->string('mac_id');
            $table->string('rfid_type');
            $table->integer('rfid_status');
            $table->string('tablet_id');
            $table->string('mo_id');
            $table->string('item');
            $table->integer('mo_qty');
            $table->integer('mo_status');
            $table->date('mo_start_date')->nullable();
            $table->time('mo_start_time')->nullable();
            $table->string('tht');
            $table->integer('finish');
            $table->integer('ng');
            $table->integer('human');
            $table->integer('acture_human');
            $table->string('start_line');
            $table->string('start_profile');
            $table->string('start_antenna_id');
            $table->date('start_date')->nullable();
            $table->time('start_time')->nullable();
            $table->time('first_one_time')->nullable();
            $table->string('s_tct');                        //下班未完工(插單)即時生產力
            $table->string('serial_no');                    //跨天生產線別不同時記錄
            $table->date('change_date')->nullable();
            $table->string('change_time');
            $table->string('change_line_id');
            $table->string('line_name');
            $table->string('change_profile');
            $table->string('s_cl_start_time');
            $table->string('s_cl_end_time');
            $table->string('s_change_lead_time');
            $table->integer('s_acture_human');
            $table->time('cl_start_time')->nullable();
            $table->time('cl_end_time')->nullable();
            $table->string('change_lead_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_off_works');
    }
}
