<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupAntennasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_antennas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('antenna_id');           //天線 ID
            $table->string('antenna_name');         //天線名稱
            $table->string('antenna_type');         //天線類型 (1開始 2完成 3錄入)
            $table->string('port_no');              //PORT號
            $table->string('port_id');              //PORT ID
            $table->string('device_id');            //判斷 ReaderID TabletID
            $table->string('org_id');               //廠別
            $table->string('routing');              //製程類別
            $table->string('line_id');              //線別 ID
            $table->string('profile');              //設備整合識別碼
            $table->string('note');                 //備註
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_antennas');
    }
}
