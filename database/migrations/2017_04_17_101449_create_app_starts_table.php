<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppStartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_starts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_start');           //訊息碼
            $table->integer('type');                //1開線人數 2完工無開始 3無G開始 4跨天生產
            $table->string('profile');              //設備整合識別碼
            $table->string('line_id');
            $table->string('org_id');
            $table->string('routing');
            $table->string('person_id');
            $table->string('name');
            $table->string('role_id');
            $table->string('device_id');
            $table->integer('human');
            $table->integer('acture_human');
            $table->string('mo_id');
            $table->string('item');
            $table->integer('mo_qty');
            $table->string('mac_id');
            $table->string('rfid_type');
            $table->integer('rfid_status');
            $table->string('start_line');
            $table->string('start_profile');
            $table->string('start_antenna_id');
            $table->dateTime('start_datetime')->nullable();
            $table->time('first_one_time')->nullable();               // 該製令第一筆開始時間
            $table->dateTime('mo_start_datetime')->nullable();        // 製令開始日期
            $table->string('end_line');                   // 完工線別
            $table->string('end_profile');
            $table->string('end_antenna_id');
            $table->dateTime('end_datetime')->nullable();             // 完工日期
            $table->string('tht');
            $table->date('change_date')->nullable();                  // 換線日期
            $table->time('change_time')->nullable();                  // 換線時間
            $table->string('change_line_id');               // 換線別ID
            $table->string('line_name');                    // 換線別
            $table->string('change_profile');               // 設備整合識別碼
            $table->string('serial_no');                    // 生產序號
            $table->date('cl_start_time')->nullable();                // 換模開始時間
            $table->time('cl_end_time')->nullable();                  // 換模結束時間
            $table->string('change_lead_time');             // 換線換模時間
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_starts');
    }
}
