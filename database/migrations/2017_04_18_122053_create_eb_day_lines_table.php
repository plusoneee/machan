<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbDayLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eb_day_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('line_id');
            $table->string('company_id');
            $table->string('org_id');
            $table->string('routing');
            $table->string('dashboard_id');
            $table->string('profile');
            $table->Integer('status');
            $table->string('eb_qty');
            $table->string('line_mo_qty');
            $table->string('rank');
            $table->Integer('day_accumulator_start');
            $table->Integer('day_accumulator_fin');
            $table->Integer('day_ng');
            $table->Integer('day_qty');
            $table->string('day_first_pass_rate');
            $table->string('day_change_lead_time');
            $table->string('day_suspend_time');
            $table->string('mo_start_time');
            $table->string('mo_finish_time');
            $table->Integer('finish');
            $table->Integer('prod_qty');
            $table->Integer('fin_qty');
            $table->Integer('nf_qty');
            $table->string('change_lead_time');
            $table->string('cl_start_time');
            $table->string('cl_end_time');
            $table->string('first_pass_rate');
            $table->Integer('acture_human');
            $table->string('tht');
            $table->string('ht');
            $table->string('tct');
            $table->string('ct');
            $table->string('qty_per_hr');
            $table->string('productivity');
            $table->string('reach_rate');
            $table->string('f_completion_time');
            $table->Integer('target_qty');
            $table->string('suspend_rate');
            $table->string('std_change_lead');
            $table->string('equivalents');
            $table->string('date');
            $table->string('mo_id');
            $table->string('item');
            $table->Integer('qty');
            $table->string('completion_date');
            $table->string('so_id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->Integer('mo_status');
            $table->Integer('accumulator_start');
            $table->Integer('accumulator_fin');
            $table->string('immediate_tct');
            $table->string('immediate_ct');
            $table->string('immediate_qty_per_hr');
            $table->string('immediate_productivity');
            $table->string('capacity_rate');
            $table->string('color');
            $table->string('immediate_equivalents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eb_day_lines');
    }
}
