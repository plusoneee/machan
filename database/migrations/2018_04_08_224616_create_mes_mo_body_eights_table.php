<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoBodyEightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_body_eights', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->string('factory_id');
            $table->string('line_id');
            $table->integer('mo_status');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->integer('prod_qty');
            $table->integer('fin_qty');
            $table->integer('accu_cross_fin_qty');
            $table->string('rest_time');
            $table->string('work_off_rest_time');
            $table->string('mix_rest_time');
            $table->string('sep_rest_time');
            $table->string('accu_sep_rest_time');
            $table->string('cross_day_rest_time');
            $table->string('accu_cross_day_rest');
            $table->string('accu_cross_fac_rest');
            $table->string('change_time');
            $table->string('accu_change_time');
            $table->string('insert_change_time');
            $table->string('accu_insert_change_time');
            $table->string('cross_change_time');
            $table->string('accu_cross_change_time');
            $table->double('delta_t');
            $table->double('sum_tct');
            $table->double('insert_delta_tct');
            $table->double('mix_sum_tct');
            $table->double('ng_delta_tct');
            $table->double('sep_sum_tct');
            $table->double('non_sep_delta_tct');
            $table->double('con_day_sum_tct');
            $table->double('non_con_delta_tct');
            $table->double('cross_fac_sum_tct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_body_eights');
    }
}
