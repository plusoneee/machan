<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_registers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mac_id');                   // RFID MAC
            $table->string('tag_id');                   // RFID TAG 序號
            $table->string('mo_id');                    // 製令單號
            $table->string('rfid_type');                // B一般 G開始 R完工
            $table->string('company_id');               // 公司別
            $table->string('factory_id');               // 廠別
            $table->integer('rfid_status');             // RFID 狀態
            $table->date('record_date')->nullable();              // 錄入日期
            $table->date('erase_date')->nullable();               // 消除日期
            $table->date('disable_date')->nullable();             // 失效日期
            $table->Integer('enable')->default(1);      // 有效
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_registers');
    }
}
