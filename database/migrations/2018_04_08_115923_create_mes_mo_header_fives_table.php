<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoHeaderFivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_header_fives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('factory_id');
            $table->string('line_id');
            $table->integer('mo_status');
            $table->string('mo_id');
            $table->integer('group_id');
            $table->double('delta_t');
            $table->date('cross_start_date')->nullable();
            $table->time('cross_start_time')->nullable();
            $table->date('cross_finish_date')->nullable();
            $table->time('cross_finish_time')->nullable();
            $table->integer('prod_qty');
            $table->integer('fin_qty');
            $table->string('rest_time');
            $table->string('accu_rest_time');
            $table->time('cl_start_time')->nullable();
            $table->time('cl_end_time')->nullable();
            $table->string('sep_change_time');
            $table->string('insert_change_time');
            $table->double('sep_sum_tct');
            $table->double('non_sep_delta_tct');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_header_fives');
    }
}
