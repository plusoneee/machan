<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesMoRfidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_mo_rfids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mo_status');               //製令狀態
            $table->string('serial_no');                //生產序號
            $table->time('cl_start_time')->nullable();            //換模開始時間
            $table->time('cl_end_time')->nullable();              //換模結束時間
            $table->string('mac_id');                   //MAC ID
            $table->string('rfid_type');                //B一般 G開始 R完工
            $table->integer('rfid_status');
            $table->string('mo_id');                    //製令單號
            $table->string('item');                     //物料名稱
            $table->integer('qty');                     //生產數量
            $table->string('so_id');                    //來源訂單號
            $table->string('customer_id');
            $table->string('customer_name');
            $table->string('start_line');
            $table->string('start_profile');
            $table->string('start_antenna_id');
            $table->dateTime('start_datetime')->nullable();
            $table->string('end_line');
            $table->string('end_profile');
            $table->string('end_antenna_id');
            $table->dateTime('end_datetime')->nullable();
            $table->string('qc_pass');
            $table->string('qc');
            $table->integer('finish');
            $table->time('finish_time')->nullable();
            $table->Integer('ng');
            $table->string('except_reason');
            $table->time('ex_start_time')->nullable();
            $table->time('ex_end_time')->nullable();
            $table->integer('suspend');
            $table->integer('code');
            $table->string('notification_id');
            $table->dateTime('ntf_date_time')->nullable();
            $table->string('notification');
            $table->string('dashboard_id');
            $table->string('immediate_tct');
            $table->string('immediate_ct');
            $table->string('immediate_qty_per_hr');
            $table->string('immediate_productivity');
            $table->string('capacity_rate');
            $table->string('color');
            $table->string('immediate_equivalents');
            $table->string('remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_mo_rfids');
    }
}
