<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesUnfinishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_unfinishes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code_nf');             //訊息碼
            $table->string('red_mac_id');           //完工RFID MAC
            $table->integer('rfid_type');
            $table->integer('nf_type');             //1未投產尾數 2欠料尾數 3異常尾數
            $table->integer('mo_qty');
            $table->string('rt_end_line');          //Red tag完工線別
            $table->string('rt_end_profile');       //Red tag完工設備識別碼
            $table->string('re_antenna_id');        //Red tag完工讀取設備號
            $table->date('rt_end_date')->nullable();
            $table->time('rt_end_time')->nullable();
            $table->integer('nf_status');           //1新增 2處理中 3完工 4結案
            $table->string('mac_id');
            $table->string('record_id');            //錄入ID
            $table->date('date')->nullable();
            $table->string('mo_id');
            $table->string('item');
            $table->string('so_id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->string('start_line');
            $table->string('start_profile');
            $table->string('start_antenna_id');
            $table->date('start_date')->nullable();
            $table->time('start_time')->nullable();
            $table->string('end_line');
            $table->string('end_profile');
            $table->string('end_antenna_id');
            $table->date('end_date')->nullable();
            $table->time('end_time')->nullable();
            $table->string('qc_pass');
            $table->string('qc');
            $table->integer('finish');
            $table->time('finish_time')->nullable();
            $table->integer('ng');
            $table->string('except_reason');
            $table->time('ex_start_time')->nullable();
            $table->time('ex_end_time')->nullable();
            $table->string('suspend');
            $table->integer('mo_status');
            $table->string('code');
            $table->string('notification_id');
            $table->dateTime('ntf_data_time')->nullable();
            $table->string('notification');
            $table->integer('rfid_status');
            $table->string('dashboard_id');
            $table->string('remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_unfinishes');
    }
}
