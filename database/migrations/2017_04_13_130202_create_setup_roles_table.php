<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('role_id');          //角色ID
            $table->string('role_name');        //角色名稱
            $table->Integer('active');          // 1有效 0無效
            $table->string('note');             //備註
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_roles');
    }
}
