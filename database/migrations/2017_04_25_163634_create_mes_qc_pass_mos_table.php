<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesQcPassMosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mes_qc_pass_mos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mac_id');
            $table->string('record_id');
            $table->string('rfid_type');
            $table->integer('rfid_status')->default(1);
            $table->string('company_id');
            $table->string('factory_id');
            $table->date('date')->nullable();
            $table->string('end_line');
            $table->string('end_profile');
            $table->string('end_antenna_id');
            $table->dateTime('end_datetime')->nullable();
            $table->string('mo_id');
            $table->string('item');
            $table->integer('qty');
            $table->string('completion_date');
            $table->string('so_id');
            $table->string('customer_id');
            $table->string('customer_name');
            $table->integer('mo_status')->default(1);
            $table->string('line_id');
            $table->string('line_name');
            $table->string('org_id');
            $table->string('routing');
            $table->string('profile');
            $table->string('dashboard_id');
            $table->string('dashboard_name');
            $table->integer('accumulator_qty');
            $table->integer('accumulator_non_print');
            $table->string('qc_pass');
            $table->string('qc');
            $table->date('print_date')->nullable();
            $table->time('print_time')->nullable();
            $table->date('force_print_date')->nullable();
            $table->time('force_print_time')->nullable();
            $table->date('reprint_date')->nullable();
            $table->time('reprint_time')->nullable();
            $table->integer('printed');
            $table->integer('force_print');
            $table->integer('reprint');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mes_qc_pass_mos');
    }
}
