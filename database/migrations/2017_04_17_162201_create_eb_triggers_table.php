<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbTriggersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eb_triggers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trigger_id');
            $table->string('trigger_name');
            $table->string('module');
            $table->string('action_description');
            $table->string('judgement_description');
            $table->string('eb_qty');
            $table->string('line_mo_qty');
            $table->Integer('priority');
            $table->string('rank');
            $table->Integer('refresh_trigger');
            $table->Integer('close');
            $table->Integer('close_trigger');
            $table->string('start_eb_rank');
            $table->string('line_mo_rank');
            $table->string('end_eb_rank');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eb_triggers');
    }
}
