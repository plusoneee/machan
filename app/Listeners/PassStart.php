<?php

namespace App\Listeners;

use App\Events\RfidStart;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Entities\Notification;
use App\Entities\SetupReader;

class PassStart
{    
    protected $request;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->request = request();
    }

    /**
     * Handle the event.
     *
     * @param  RfidStart  $event
     * @return void
     */
    public function handle(RfidStart $event)
    {
        $mac_id = $this->request->mac_id;
        $reader_id = $this->request->reader_id;
        $readerInfo = SetupReader::where('reader_id', $reader_id)
                        ->first();
        if (!$this->checkNotifyStatus(new Notification, $this->request)) {
            return Notification::create([
                'code' => 'N004',
                'infor_date' => date('Y-m-d'),
                'infor_time' => date('H:i:s'),
                'mac_id' => $mac_id,
                'line_name' => $readerInfo->relatedLine->line_name
            ]);
        } 
    }

    // 確認當日通知是否寫入
    private function checkNotifyStatus(Notification $notify, $request)
    {
        $data = $request->all();
        return $notify->where('mac_id', $data['mac_id'])
            ->where('infor_date', date('Y-m-d'))
            ->where('code', 'N004')
            ->first() ? true : false;
    }
}
