<?php

namespace App\Listeners;

use App\Events\RfidEnd;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Entities\Notification;
use App\Entities\SetupLine;

class PassEnd
{
    protected $request;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->request = request();
    }

    /**
     * Handle the event.
     *
     * @param  RfidEnd  $event
     * @return void
     */
    public function handle(RfidEnd $event)
    {
        $user = auth()->user();
        $lineInfo = SetupLine::where('company_id', $user->company_id)
                ->where('line_id', $user->line_id)
                ->where('routing', $user->routing)
                ->where('org_id', $user->org_id)
                ->first();
        $mac_id = $this->request->mac_id;
        if (!$this->checkNotifyStatus(new Notification, $this->request)) {
            return
                Notification::create([
                    'code' => 'N008',
                    'infor_date' => date('Y-m-d'),
                    'infor_time' => date('H:i:s'),
                    'mac_id' => $mac_id,
                    'line_name' => $lineInfo->line_name
                ]);
        }
    }

    // 確認當日通知是否寫入
    private function checkNotifyStatus(Notification $notify, $request)
    {
        $data = $request->all();
        return $notify->where('mac_id', $data['mac_id'])
            ->where('infor_date', date('Y-m-d'))
            ->where('code', 'N008')
            ->first() ? true : false;
    }
}
