<?php

namespace App\Listeners;

use DB;
use App\Events\QcPass;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Entities\MesRegister;
use App\Entities\Notification;
use App\Entities\SetupNotification;

class Reprint
{
    protected $request;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = request();
    }

    /**
     * Handle the event.
     *
     * @param  QcPass  $event
     * @return void
     */
    public function handle(QcPass $event)
    {
        $data = $this->request->all();
        $qcPass = $data['qc_pass'][0];
        $qcInfo = DB::table('mes_qc_pass_mos')
                    ->where('qc_pass', $qcPass)
                    ->first();
        $notiInfo = SetupNotification::where('code', 'N017')
                        ->first();
        Notification::create([
            'code' => 'N017',
            'infor_date' => date('Y-m-d'),
            'infor_time' => date('H:i:s'),
            'role_name' => $notiInfo->role_name,
            'mac_id' => '',
            'rfid_type' => '',
            'company_id' => '',
            'factory_id' => '',
            'rfid_status' => '',
            'mo_id' => $qcInfo->mo_id,
            'item' => $qcInfo->item,
            'qty' => $qcInfo->qty,
            'completion_date' => $qcInfo->completion_date,
            'so_id' => $qcInfo->so_id,
            'customer_id' => $qcInfo->customer_id,
            'customer_name' => $qcInfo->customer_name,
            'line_id' => $qcInfo->line_id,
            'line_name' => $qcInfo->line_name
        ]);
    }
}
