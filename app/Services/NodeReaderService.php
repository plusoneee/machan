<?php

namespace App\Services;

use Curl\Curl;

/**
 * 處理與 Reader 及 NodeJS 溝通
 */

class NodeReaderService
{
    private $curl;

    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
    }

    /**
     * IOT API 計算 TCT
     */
    public function getTct($mo_id)
	{
		$url = 'http://'.env('NODE_IP').'/TCT/immediate';
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->post($url, array("mo_id" => $mo_id));
		$productData = json_decode($this->curl->rawResponse);
		return $productData;
    }
    
    /**
     * 關閉 Start-Reader
     */
	public function stopStartReader($line)
	{
		$url = 'http://'.env('READER_IP').'/start/stop';
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->post($url, ['line' => $line]);
		$response = json_decode($this->curl->rawResponse);
		return !is_null($response) ? $response : 1;
	}

    /**
     * 關閉 End-Reader
     */
	public function stopEndReader($line)
	{
		$url = 'http://'.env('READER_IP').'/end/stop';
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->post($url, ['line' => $line]);
		$response = json_decode($this->curl->rawResponse);
		return !is_null($response) ? $response : 1;
	}

    /**
     * 啟動 Start-Reader
     */
	public function runStartReader($reader_id, $line)
	{
		$url = 'http://'.env('READER_IP').'/start/rfid';
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->post($url, ['reader_id' => $reader_id, 'line' => $line]);
		$response = json_decode($this->curl->rawResponse);
		return !is_null($response) ? $response : 1;
	}

    /**
     * 啟動 End-Reader
     */
	public function runEndReader($reader_id, $line)
	{
		$url = 'http://'.env('READER_IP').'/end/rfid';
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->post($url, ['reader_id' => $reader_id, 'line' => $line]);
		$response = json_decode($this->curl->rawResponse);
		return !is_null($response) ? $response : 1;
	}

    /**
     * 確認 Start-Reader 是否啟動
     */
	public function checkStart($line)
	{
		$url = 'http://'.env('READER_IP').'/start/test';
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->post($url, ['line' => $line]);
		$response = json_decode($this->curl->rawResponse);
		return $response ? $response : 1;
	}

	/**
     * 確認 End-Reader 是否啟動
     */
	public function checkEnd($line)
	{
		$url = 'http://'.env('READER_IP').'/end/test';
		$this->curl->setHeader('Content-Type', 'application/json');
		$this->curl->post($url, ['line' => $line]);
		$response = json_decode($this->curl->rawResponse);
		return $response ? $response : 1;
	}
}