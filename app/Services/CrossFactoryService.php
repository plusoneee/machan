<?php

namespace App\Services;

use Curl\Curl;

/**
 * 處理與 HQ SERVER API 溝通
 */

class CrossFactoryService
{
    private $curl;

    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
    }

    public function connectHQ()
    {
        $this->curl->setHeader('Content-Type', 'application/json');
        $this->curl->get('http://'.env('HQ_IP').'/api/test');
        // dd(json_decode($this->curl->rawResponse));
    }
}