<?php

namespace App\Traits;

use App\Entities\SetupLine;

/**
 * 處理看板線別
 */
trait BoardProfile
{
    public function checkProfile($profile)
    {
        return SetupLine::where('profile', $profile)
            ->first();
    }

    public function compareNumber(array $params)
    {
        if ($params['first_condition'] <= $params['second_condition']) {
            return 1;
        }
    }
}