<?php

namespace App\Providers;

use App\Events\PushNotification;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\RfidStart' => [
            'App\Listeners\PassStart',
        ],
        'App\Events\RfidEnd' => [
            'App\Listeners\PassEnd',
        ],
        'App\Events\QcPass' => [
            'App\Listeners\Reprint',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
