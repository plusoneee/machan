<?php

namespace App\Repositories;

use App\Entities\SetupPrint;
use App\Entities\SetupTablet;
use App\Entities\SetupEboard;
use App\Entities\SetupAntenna;

class HardwareRepository
{
    public function getTablets($search = null)
    {
        $entity = new SetupTablet;
        if ($search) {
            $collection = $entity->where('tablet_id', 'LIKE', '%'.$search.'%')
                            ->get();
            return $this->insertColumn($collection);
        }
        return $this->insertColumn($entity->get());
    }

    public function getBoards($search = null)
    {
        $entity = new SetupEboard;
        return $search ? $entity->where('dashboard_name', 'LIKE', '%'.$search.'%')
            ->get() : $entity->get();
    }

    public function getPrints($search = null)
    {
        $entity = new SetupPrint;
        if ($search) {
            return $entity->where('b_print_name', 'LIKE', '%'.$search.'%')
                ->get();
        }
        return 
            $entity->get()->each(function ($item) {
                $item->b_print_type = $item->profile === 'none' ? '錄入區' : '完工區';
            });
    }

    public function getAntennas()
    {
        return SetupAntenna::get();
    }

    /**
     * Add new column into collection
     */
    private function insertColumn($collection)
    {
        return
            $collection->each(function ($item) {
                $item->line_name = $item->line ? $item->line->line_name : '';
                $item->antenna_name = $item->antenna ? $item->antenna->antenna_name : '';
            });
    }

    /**
     * Tablet參數
     */
    public function createTablet(array $data)
    {
        return SetupTablet::create($data);
    }

    public function updateTablet(array $data, $tablet_id)
    {
        $tablet = SetupTablet::where('tablet_id', $tablet_id)
                    ->first();
        return $tablet ? $tablet->update($data) : false;
    }

    public function deleteTablet($tablet_id)
    {
        $data = SetupTablet::where('tablet_id', $tablet_id)
                    ->first();
        return !$data ? false : $data->delete();
    }

    /**
     * 電子看板參數
     */ 
    public function createBoard(array $data)
    {
        return SetupEboard::create($data);
    }

    public function updateBoard(array $data, $id)
    {
        $board = SetupEboard::find($id);
        if (!$board) {
            return false;
        }
        return $board->update($data);
    }

    public function deleteBoard($id)
    {
        $data = SetupEboard::find($id);
        return !$data ? false : $data->delete();
    }

    /**
     * 藍芽印表機參數
     */
    public function createPrint(array $data)
    {
        return SetupPrint::create($data);
    }

    public function updatePrint(array $data, $id)
    {
        $print = SetupPrint::find($id);
        if (!$print) {
            return false;
        }
        return $print->update($data);
    }

    public function deletePrint($id)
    {
        $print = SetupPrint::find($id);
        if (!$print) {
            return false;
        }
        return $print->delete();
    }

    /**
     * Antenna 參數設定
     */
    public function createAntenna(array $data)
    {
        return SetupAntenna::create($data);
    }

    public function updateAntenna(array $data, $id)
    {
        # code...
    }

    public function deleteAntenna($id)
    {
        
    }
}