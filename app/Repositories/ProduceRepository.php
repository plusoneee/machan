<?php

namespace App\Repositories;

use App\Entities\MesRecord;

class ProduceRepository
{
    // 檢查錄入製令 取得G B R錄入數量
    public function getRecordTag($mo_id)
    {
        $data = [];
        $types = ['G', 'B', 'R'];
        $record = MesRecord::where('mo_id', $mo_id)
                    ->get();
        foreach ($types as $key => $type) {
            $data[$type] = count($record->where('rfid_type', $type));
        }
        return $data;
    }
}