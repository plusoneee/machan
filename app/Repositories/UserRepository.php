<?php

namespace App\Repositories;

use App\Entities\User;

class UserRepository
{
    // 使用者註冊
    public function create(array $data)
    {
        $data['password'] = bcrypt($data['password']);
        return User::create($data);
    }

    public function find()
    {
        return auth()->user();
    }

    // 修改個人資訊
    public function update(User $user, array $data)
    {
        return $user->update($data);
    }
}