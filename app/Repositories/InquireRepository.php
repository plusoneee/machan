<?php

namespace App\Repositories;

use DB;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesRecord;
use App\Entities\MesRegister;
use App\Entities\MesMoRfid;
use App\Entities\MesQcPassMo;
use App\Entities\SetupLine;
use App\Entities\SetupTablet;
use App\Entities\SetupSuspend;
use App\Entities\SetupNonFinished;
use App\Entities\Notification;

class InquireRepository
{
	/**
	 * RFID TAG 數據
	 */
	public function rfidInfo($mac_id)
	{
		$data = [];
		$mac = MesRegister::where('mac_id', $mac_id)
					->first();
		$tagId = $this->rfidTagAllocate($mac);	

		// 依據關聯 取得RFID歷史紀錄
		$tagHistory = $mac->relatedRfid()
						->selectRaw('id, customer_name, rfid_status, rfid_type, so_id, item, mo_id, ng, date(start_datetime) AS start_date, time(start_datetime) AS start_time, date(end_datetime) AS end_date, time(end_datetime) AS end_time')
						->orderBy('id', 'DESC')
						->get();	
		
		// 依據歷史紀錄 取得最新RFID
		$new = $this->getNewRfid($mac, $tagHistory);						
		
		foreach ($tagHistory as $key => $rfid) {
			$newTag = $tagHistory->first();	// 取得最新RFID
			$newId = $newTag->id;
			if ($mac->rfid_status === 2) {	// 過濾最新RFID之id
				$data[] = [
					'customer_name' => $rfid->customer_name,
					'so_id' => $rfid->so_id,
					'item' => $rfid->item,
					'item_name' => '',
					'mo_id' => $rfid->mo_id,
					'start_date' => !is_null($rfid->start_date) ? $rfid->start_date : '',
					'start_time' => !is_null($rfid->start_time) ? $rfid->start_time : '',
					'end_date'=> !is_null($rfid->end_date) ? $rfid->end_date : '',
					'end_time' => !is_null($rfid->end_time) ? $rfid->end_time : '',
				];
			}
			
			if ($mac->rfid_status !== 2 && $rfid->id !== $newId) {
				$data[] = [
					'customer_name' => $rfid->customer_name,
					'so_id' => $rfid->so_id,
					'item' => $rfid->item,
					'item_name' => '',
					'mo_id' => $rfid->mo_id,
					'start_date' => !is_null($rfid->start_date) ? $rfid->start_date : '',
					'start_time' => !is_null($rfid->start_time) ? $rfid->start_time : '',
					'end_date'=> !is_null($rfid->end_date) ? $rfid->end_date : '',
					'end_time' => !is_null($rfid->end_time) ? $rfid->end_time : '',
				];
			}
		}
		return [$data, $new, $tagId];
	}

	/**
	 * TAG 最新資訊
	 */
	private function getNewRfid($rfid, $tags)
	{
		if ($tags->count() !== 0) {
			$newTag = $tags->first();	// 取得最新TAG
			if ($rfid->rfid_status !== 2) {
				return [
					'ng' => $newTag->ng,
					'notify' => $newTag->ng === 1 ? $this->getUnfinishNotify($newTag->mo_id) : -1,
					'tag_id' => $rfid->tag_id,
					'mac_id' => $rfid->mac_id,
					'customer_name' => $newTag->customer_name,
					'so_id' => $newTag->so_id,
					'item' => $newTag->item,
					'item_name' => '',
					'mo_id' => $newTag->mo_id,
					'rfid_status' => $newTag->rfid_status,
					'rfid_type' => $newTag->rfid_type
				];
			}	
		}
		return [
			'ng' => 0,
			'notify' => -1,
			'tag_id' => $rfid->tag_id,
			'mac_id' => $rfid->mac_id,
			'customer_name' => '',
			'so_id' => '',
			'item' => '',
			'item_name' => '',
			'mo_id' => $rfid->mo_id,
			'rfid_status' => $rfid->rfid_status,
			'rfid_type' => $rfid->rfid_type
		];
	}

	/**
	 * 回傳尾數異常通知流水號 若無則回傳-1
	 */
	private function getUnfinishNotify($mo_id)
	{
		$notify = Notification::where('code', 'N011')
			->where('mo_id', $mo_id)
			->first();
		return $notify ? $notify->id : -1;
	}

	/**
	 * RFID TAG 配號機制
	 */
	private function rfidTagAllocate(MesRegister $rfidInfo)
	{	
		// RFID 數據API 檢查是否有tag id
		if (empty($rfidInfo->tag_id)) {
			$factoryId = $rfidInfo->factory_id;
			$year = substr(date('Y-m-d'), 2, 2);
			$serialNumber = str_pad($rfidInfo->id, 5, '0', STR_PAD_LEFT);
			$rfidType = $rfidInfo->rfid_type;
			$rfidInfo->tag_id = $factoryId.$year.$serialNumber.$rfidType;
			$rfidInfo->save();
		}
		return $rfidInfo->tag_id;
	}

	/**
	 * 製令報工數據 (明細)
	 */
	public function moInfo($mo_id)
	{
		$data = [];
		$nonOnline = [];
		$moInfos = DB::table('mes_rfids')
				->where('mo_id', $mo_id)	
				->selectRaw('mac_id, start_line, end_line, date(start_datetime) AS start_date, date(end_datetime) AS end_date, time(start_datetime) AS start_time, time(end_datetime) AS end_time, qc_pass, rfid_type')
				->orderBy('start_datetime', 'ASC')
				->get();
		foreach ($moInfos as $key => $moInfo) {
			if (!is_null($moInfo->start_date)) {	// 已上線
				if (is_null($moInfo->end_date)) {
					$moInfo->end_date = '';
					$moInfo->end_time = '';
				}
				array_push($data, $moInfo);
			} else {						// 未上線
				$moInfo->start_date = '';
				$moInfo->start_time = '';
				$moInfo->end_date = '';
				$moInfo->end_time = '';
				array_push($nonOnline, $moInfo);
			}
		}
		return array_merge($data, $nonOnline);	// 陣列合併
	}

	/**
	 * 製令報工數據 (清單)
	 */
	public function moList($params)
	{
		$status = (isset($params['status'])) ? $params['status'] : '%';
		$mo_id = (isset($params['mo_id'])) ? $params['mo_id'] : '%';
		return DB::table('mes_mos')
			->whereDate('date', $params['date'])
			->where('mo_status', 'like', $status)
			->where('mo_id', 'like', $mo_id)
			->orderBy('start_time', 'DESC')
			->selectRaw('customer_name, mo_id, serial_no, qty AS order_qty, qty, accumulator_start, accumulator_fin, mo_status AS status, date')
			->get();
	}

	/**
	 * QC PASS 模組
	 */
	public function qcpassInfo($params)
	{
		$data = [];
		$qcPass = [];
		$result = [];

		// 未被列印 則回傳1
		// 只傳入QC PASS
		if (empty($params['mo_id']) && !empty($params['qc_pass'])) {
			$qcpassInfo = DB::table('mes_qc_pass_mos')
						->where('qc_pass', $params['qc_pass'])
						->where('printed', '1')
						->first();
			if (!$qcpassInfo) {
				return 1;
			} else {
				// 強制列印：輸出強制列印date/time
				// 一般/重複/預先：輸出一般列印date/time
				$printInfo = $this->checkPrintStatus($qcpassInfo->mo_id, $qcpassInfo->mac_id);
				$data = [
					'customer_name' => $qcpassInfo->customer_name,
					'item' => $qcpassInfo->item,
					'item_name' => '',
					'mac_id' => $qcpassInfo->mac_id,
					'qty' => $qcpassInfo->qty,
					'status' => $qcpassInfo->printed,
					'print_date' => $printInfo['print_date'],
					'print_time' => $printInfo['print_time'],
					'qc_pass' => $qcpassInfo->qc_pass,
					'mo_id' => $qcpassInfo->mo_id
				];
				array_push($result, $data);
				return $result;
			}
		}

		// 只傳入製令單號
		if (!empty($params['mo_id']) && empty($params['qc_pass'])) {
			$qcpassInfo = DB::table('mes_qc_pass_mos')
						->where('mo_id', $params['mo_id'])
						->first();
			$qcDatas = MesQcPassMo::where('mo_id', $params['mo_id'])
						->where('printed', '1')
						->orderByRaw('CONVERT(SUBSTRING_INDEX(qc_pass, "_", -1), UNSIGNED INTEGER) ASC')
						->get();
			if (!$qcpassInfo) {
				return 1;
			} else {
				$data = [
					'customer_name' => $qcpassInfo->customer_name,
					'item' => $qcpassInfo->item,
					'item_name' => '',
					'qty' => $qcpassInfo->qty,
					'status' => $qcpassInfo->printed,
					'print_date' => !is_null($qcpassInfo->print_date) ? $qcpassInfo->print_date : '',
					'print_time' => !is_null($qcpassInfo->print_time) ? $qcpassInfo->print_time : ''
				];

				foreach ($qcDatas as $key => $qcData) {
					$printInfo = $this->checkPrintStatus($qcData->mo_id, $qcData->mac_id);
					$qcPass[] = [
						'mac_id' => $qcData->mac_id,
						'rfid_status' => $qcData->rfid_status,
						'qc_pass' => $qcData->qc_pass,
						'print_date' => !is_null($printInfo['print_date']) ? $printInfo['print_date'] : '',
						'print_time' => !is_null($printInfo['print_time']) ? $printInfo['print_time'] : ''
					];
				}
				array_push($result, $data, $qcPass);
				return $result;
			}
		}
	}

	/**
	 * 判斷TAG之列印種類(一般、強制、重複)
	 */
	private function checkPrintStatus($mo_id, $mac_id)
	{
		$printInfo = MesQcPassMo::where('mo_id', $mo_id)
						->where('mac_id', $mac_id)
						->first();
		if ($printInfo->force_print_time) {
			return [
				'print_date' => $printInfo->force_print_date,
				'print_time' => $printInfo->force_print_time
			];
		}
		return [
			'print_date' => $printInfo->print_date,
			'print_time' => $printInfo->print_time
		];
	}

	/**
	 * 強制列印清單
	 */
	public function forceList()
	{
		return MesMo::where('finish', 1)
			->whereColumn('accumulator_fin', '<', 'qty')
			// ->whereDate('updated_at', date('Y-m-d'))
			->selectRaw('customer_name, mo_id, qty AS order_qty, accumulator_fin AS finish_qty')
			->get();
	}

	/**
	 * 強制列印明細
	 */
	public function forceInfo($mo_id)
	{
		$nonPrints = MesQcPassMo::with('relatedMo')
						// ->whereDate('date', date('Y-m-d'))
						->where('mo_id', $mo_id)
						->where('rfid_type', 'B')
						->where('printed', 0)
						->get();
		$moInfo = count($nonPrints) > 0 ? $nonPrints->first() : '';
		$data = $nonPrints->map(function ($item, $key) {
			return [
				'mac_id' => $item->mac_id,
				'qc_pass' => $item->qc_pass,
			];
		});
		return [
			'data' => $data,
			'customer_name' => $moInfo ? $moInfo->customer_name : '',
			'accumulator_start' => $moInfo ? $moInfo->relatedMo->accumulator_start : 0,
			'accumulator_fin' => $moInfo ? $moInfo->relatedMo->accumulator_fin : 0,
			'qty' => $moInfo ? $moInfo->relatedMo->qty : 0,
			'item' => $moInfo ? $moInfo->relatedMo->item : '',
		];
	}

	/**
	 * 預先列印明細
	 */
	public function advancePrint($mo_id)
	{
		return MesQcPassMo::where('mo_id', $mo_id)
			->where('rfid_type', 'B')
			->where('rfid_status', 1)
			->select('mac_id', 'rfid_status', 'qty', 'customer_name', 'item')
			->get();
	}

	/**
	 * 取得線上製令
	 */
	public function onlineMo()
	{
		$data = DB::table('mes_mos')
				->selectRaw('date(updated_at), mo_id, customer_name, accumulator_start, accumulator_fin')
				// ->whereColumn('accumulator_fin', '<', 'qty')
				->where('date', '<>', null)
				->where('finish', '<>', 1)
				->get();
				
		return
			$data->map(function ($item) {
				$qcPass = $this->getLastQcPass($item->mo_id);
				return [
					'mo_id' => $item->mo_id,
					'customer_name' => $item->customer_name,
					'qc_pass' => !is_null($qcPass) ? $qcPass->qc_pass : '',
					'accumulator_start' => $item->accumulator_start,
					'accumulator_fin' => $item->accumulator_fin,
				];
			});
	}

	/**
	 * 取得最新完工tag
	 */
	public function getLastQcPass($mo_id)
	{
		return MesMoRfid::where('mo_id', $mo_id)
			->where('rfid_status', 4)
			->where('rfid_type', 'B')
			->where('qc_pass', '<>', '')
			->orderBy('updated_at', 'DESC')
			->first();
	}

	/**
	 * 確認該製令有無tag上線中
	 */
	public function checkOnlineTag($mo_id)
	{
		return MesQcPassMo::where('mo_id', $mo_id)
			->where('rfid_type', 'B')
			->where('rfid_status', 3)
			->count();
	}

	/**
	 * 取得消除時 製令資訊
	 */
	public function eraseMo($mo_id)
	{
		$data = MesMo::where('mo_id', $mo_id)
				->first();
		return [
			'mo_id' => $data->mo_id,
			'customer_name' => $data->customer_name,
			'qty' => $data->qty
		];
	}

	/**
	 * 回傳尾數異常原因
	 */
	public function getReasonIndex()
	{
		return SetupNonFinished::select('nf_code', 'exception')
			->get();
	}

	/**
	 * 新增尾數異常原因
	 */
	public function storeReason($exception)
	{
		$data = SetupNonFinished::create(['exception' => $exception]);
		$nf_code = 'N'.str_pad($data->id, 3, '0', STR_PAD_LEFT);
		$data->nf_code = $nf_code;
		$data->save();
		return $data;
	}

	/**
	 * 回傳除外工時原因
	 */
	public function getExceptionIndex()
	{
		return SetupSuspend::select('sp_code', 'suspend')
			->get();
	}
}
