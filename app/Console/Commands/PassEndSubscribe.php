<?php

namespace App\Console\Commands;

use DB;
use Log;
use Redis;
use Illuminate\Console\Command;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesMoRfid;
use App\Entities\MesRecord;
use App\Entities\MesQcPass;
use App\Entities\MesRegister;
use App\Entities\MesQcPassMo;
use App\Entities\MesTimeflag;
use App\Entities\AppUnfinish;
use App\Entities\AppSuspend;
use App\Entities\Notification;
use App\Entities\SetupNotification;

class PassEndSubscribe extends Command
{
    protected $table = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channel:PassEndSubscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->table = [
            'mes_records' => [],
            'mes_rfids' => [],
            'mes_mo_rfids' => [],
            'mes_qc_passes' => [],
            'mes_qc_pass_mos' => [],
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::subscribe(['end-channel'], function($message) {
            if (!is_null($message) && !empty($message)) {
                try {
                    $data = json_decode($message);
                    // 0:Red結案 1:強制結案
                    $endStatus = $data->status;
                    if ($endStatus === 0) {
                        switch ($data->rfid_type) {
                            case 'G':
                                Log::info($data->mo_id.' - '.$data->mac_id.' pass end');
                                $table = $this->tableInitialize($data, $this->table);
                                $mergeData = $this->addRepeat($data, $table);
                                $this->storeData($data->mo_id, $data->mac_id, $mergeData);
                                Log::info($data->mo_id.' - '.$data->mac_id.' pass end successfully');
                                break;
                            case 'B':
                                // 將force_time 與 print_time對調 再將qc_pass往下塞
                                $checkForceStatus = $this->checkForce($data);
                                if ($checkForceStatus) {
                                    $this->updatePrintTime($data);
                                }

                                Log::info($data->mo_id.' - '.$data->mac_id.' pass end');
                                $table = $this->tableInitialize($data, $this->table);
                                $mergeData = $this->addRepeat($data, $table);
                                $this->storeData($data->mo_id, $data->mac_id, $mergeData);
                                Log::info($data->mo_id.' - '.$data->mac_id.' pass end successfully');

                                $this->notifyNonStart($data);    // 完工無開始 (單一產品)
                                $this->handleUnfinished($data);
                                break;
                            case 'R':
                                Log::info($data->mo_id.' - '.$data->mac_id.' pass end');
                                $table = $this->tableInitialize($data, $this->table);
                                $mergeData = $this->addRepeat($data, $table);
                                $this->storeData($data->mo_id, $data->mac_id, $mergeData);
                                Log::info($data->mo_id.' - '.$data->mac_id.' pass end successfully');

                                $this->changeMoStatus($data->mo_id);    // 改變製令狀態 (含尾數資訊)
                                $this->nextChangeStart($data->profile); // 下一批製令換模開始
                                $this->notifyUnfinished($data);
                                $this->notifyNonStart($data);    // 完工無開始 (單一產品)
                                $this->notifySuspendWork($data);
                                break;
                        }
                    } elseif ($endStatus === 1) {
                        $this->changeMoStatus($data->mo_id);
                        Log::info($data->mo_id.' force closing successfully');
                        $this->notifyUnfinished($data);
                        $this->notifySuspendWork($data);  
                    }
                } catch (\Exception $e) {
                    echo date('Y-m-d H:i:s');
                    echo $e;
                }
            }
        });
    }
    
    // table 屬性初始化 定義value
    private function tableInitialize($data, $table)
    {
        $table['mes_qc_passes'] = [
            'qc' => $data->qc,
            'qc_pass' => $data->qc_pass,
        ];
        $table['mes_registers'] = [
            'rfid_status' => 4,
        ];
        $table['mes_rfids'] = [
            'end_profile' => $data->profile,
            'end_datetime' => $data->now,
            'rfid_status' => 4,
        ];
        return $table;
    }

    // 新增重複欄位
    private function addRepeat($data, $table)
    {
        $recordArray = ['rfid_status' => 4];
        $table['mes_records'] = array_merge($table['mes_records'], $recordArray);

        $recordArray = [
            'qc' => $data->qc,
            'qc_pass' => $data->qc_pass,
            'end_line' => $data->line_id,
            'end_antenna_id' => $data->antenna_id,
        ];
        $table['mes_rfids'] = array_merge($table['mes_rfids'], $recordArray);

        $recordArray = [
            'qc' => $data->qc,
            'qc_pass' => $data->qc_pass,
            'end_datetime' => $data->now,
            'rfid_status' => 4,
            'end_line' => $data->line_id,
            'end_antenna_id' => $data->antenna_id,
            'end_profile' => $data->profile,
        ];
        $table['mes_mo_rfids'] = array_merge($table['mes_mo_rfids'], $recordArray);

        $recordArray = ['rfid_status' => 4];
        $table['mes_qc_passes'] = array_merge($table['mes_qc_passes'], $recordArray);

        $recordArray = [
            'rfid_status' => 4,
            'qc' => $data->qc,
            'qc_pass' => $data->qc_pass,
            'end_line' => $data->line_id,
            'end_profile' => $data->profile,
            'end_antenna_id' => $data->antenna_id,
            'end_datetime' => $data->now,
        ];
        $table['mes_qc_pass_mos'] = array_merge($table['mes_qc_pass_mos'], $recordArray);
        return $table;
    }

    // 將資料寫入MySQL
    private function storeData($mo_id, $mac_id, $mergeData)
    {
        // 以迴圈方式 針對model進行更新
        // entities 與 tables 呼應相同資料表
        $entities = [new MesRegister, new MesRecord, new MesRfid, new MesMoRfid, new MesQcPass, new MesQcPassMo];
        $tables = ['mes_registers', 'mes_records', 'mes_rfids', 'mes_mo_rfids', 'mes_qc_passes', 'mes_qc_pass_mos'];
        foreach ($entities as $key => $entity) {
            $entity->where('mac_id', $mac_id)
                ->where('mo_id', $mo_id)
                ->orderBy('created_at', 'DESC')
                ->first()
                ->update($mergeData[$tables[$key]]); // 由於entities 與 tables 兩陣列順序相同，所以可直接使用foreach key變數
        }
    }
    

    // 一般結案與強制結案 變更製令狀態
    public function changeMoStatus($mo_id)
    {
        $tables = ['mes_rfids', 'mes_mo_rfids'];
        foreach ($tables as $key => $table) {
            $data = [
                'finish' => 1,
                'finish_time' => date('H:i:s'),
            ];
            if ($table === 'mes_mo_rfids') {
                $data = array_merge($data, ['cl_start_time' => date('H:i:s')]);
            }
            DB::table($table)
                ->where('mo_id', $mo_id)
                ->update($data);
        }

        // 異常尾數狀態
        foreach ($tables as $key => $table) {
            DB::table($table)
                ->where('mo_id', $mo_id)
                ->where('rfid_status', '<>', 4)
                ->where('rfid_type', 'B')
                ->update(['ng' => 1]);
        }

        $moData = MesMo::where('mo_id', $mo_id)
                    ->first();
        $prodQty = $this->getRfidQty($mo_id)->where('start_datetime', '<>', null)->get();
        $finQty = $this->getRfidQty($mo_id)->where('rfid_status', 4)->get();
        $nfQty = $moData->qty - count($finQty);
        $moData->update([
            'finish_date' => date('Y-m-d'),
            'finish_time' => date('H:i:s'),
            'finish' => 1,
            'first_finish_date' => date('Y-m-d'),
            'first_finish_time' => date('H:i:s'),
            'prod_qty' => count($prodQty),
            'fin_qty' => count($finQty),
            'nf_qty' => $nfQty
        ]);
        // 製令完工狀態 (有尾數 製令狀態為3；無尾數 製令狀態為5)
        $moData->update([
            'mo_status' => $nfQty === 0 ? 5 : 3 
        ]);

        // TCT TCT(HR) CT
        $startTime = $moData->date . $moData->start_time;
        $endTime = $moData->finish_date . $moData->finish_time;
        $tct = floor(strtotime($endTime) - strtotime($startTime));
        $moData->update([
            'tct' => $tct,
            'ct' => count($finQty) === 0 ? '' : $tct / count($finQty)
        ]);
    }

    // 下一批製令換模開始
    private function nextChangeStart($profile)
    {
        $checkStart = MesTimeflag::where('profile', $profile)
                        ->whereDate('date', date('Y-m-d'))
                        ->where('cl_start_time', null)
                        ->orderBy('created_at', 'desc')
                        ->first();
        // 下一批製令換模開始
        if ($checkStart) {
            MesTimeflag::create([
                'profile' => $profile,
                'type' => 2,
                'date' => date('Y-m-d'),
                'cl_start_time' => date('H:i:s'),
            ]);
        }
    }

    // 尾數TAG完工
    public function handleUnfinished($data)
    {
        $appInfo = AppUnfinish::where('mo_id', $data->mo_id)
                    ->where('mac_id', $data->mac_id)
                    ->first();
        $rfidInfo = MesRfid::where('mo_id', $data->mo_id)
                    ->where('mac_id', $data->mac_id)
                    ->first();
        
        if ($appInfo && $rfidInfo) {
            $appInfo->update([
                'start_line' => $rfidInfo->start_line,
                'start_profile' => $rfidInfo->start_profile,
                'start_antenna_id' => $rfidInfo->start_antenna_id,
                'start_date' => $rfidInfo->start_date,
                'start_time' => $rfidInfo->start_time,
                'end_line' => $data->line_id,
                'end_profile' => $data->profile,
                'end_antenna_id' => $data->antenna_id,
                'end_date' => date('Y-m-d'),
                'end_time' => date('H:i:s'),
                'qc_pass' => $data->qc_pass,
                'qc' => $data->qc,
                'finish' => 1,
                'finish_time' => date('H:i:s'),
                'rfid_status' => 4,
            ]);
        }
    }

    // 回傳該製令RFID
    private function getRfidQty($mo_id)
    {
        return MesRfid::where('mo_id', $mo_id)
            ->where('rfid_type', 'B');
    }

    // 尾數異常 N011
    public function notifyUnfinished($data)
    {
        $code = 'N011'; // 尾數異常通知代號
        $dataTmps = [];
        if ($this->checkNotifyStatus($code, $data->mo_id) === false) {
            $moInfo = MesMo::where('mo_id', $data->mo_id)
                        ->first();
            if ($moInfo->nf_qty > 0) {
                $rfids = $this->getNfTag($data->mo_id);     // 取得所有尾數blue Tag
                foreach ($rfids as $key => $rfid) {     // 將所有尾數Tag 以迴圈方式填入陣列
                    $dataTmps[$key] = $this->dataCollection($code, $data, $rfid);
                    AppUnfinish::create($dataTmps[$key]);
                }
                
                Notification::create($this->dataCollection($code, $data));     // 寫入通知資料表
            } 
        }
    }

    // 回傳該製令所有尾數TAG
    private function getNfTag($mo_id)
    {
        $moData = MesMo::with('relatedRfid')
                    ->where('mo_id', $mo_id)
                    ->first();
        $ngRfids = $moData->relatedRfid()->get();   // MesMo 關聯 MesRfid
        return
            $ngRfids->map(function ($item) {
                if ($item->rfid_type === 'B' && $item->ng === 1) {
                    return $item->mac_id;
                }    
            })->filter()->all();    // collection 過濾null
    }

    // 完工無開始 N009
    public function notifyNonStart($data)
    {
        $code = 'N009'; // 完工無開始通知代號
        if ($this->checkStart($data->mo_id, $data->mac_id)) {     
            $dataTmp = $this->dataCollection($code, $data);
            Notification::create($dataTmp);
        }
    }

    // 回傳該TAG開線時間
    private function checkStart($moid, $macid)
    {
        return MesRfid::where('mo_id', $moid)
            ->where('mac_id', $macid)
            ->where('start_antenna_id', '')
            ->orderBy('created_at', 'DESC')
            ->first();
    }

    // 除外工時 - 作業效率異常
    public function notifySuspendWork($data)
    {
        $standardTHT = 1347.2 * 1.2;
        $datas = MesRfid::where('mo_id', $data->mo_id)
                    ->where('rfid_type', 'B')
                    ->where('rfid_status', 4)
                    ->get();
        $diffTime = 0;
        foreach ($datas as $key => $data) {
            $diffTime += strtotime($data->end_time) - strtotime($data->start_time);
        }
        
        $code = 'N019';
        $actureTHT = $this->transferSeconds($diffTime);
        if ($actureTHT >= $standardTHT) {   // 若實際平均THT >= 1347.2*1.2 則觸發
            if ($this->checkNotifyStatus($code, $data->mo_id) === false) {
                $dataTmp = $this->dataCollection($code, $data);
                Notification::create($dataTmp);
                AppSuspend::create($dataTmp);
            }
        }
    }

    // 累計時間 轉換秒數
    private function transferSeconds($date)
    {
        $hours = gmdate('H', $date) * 3600;
        $minutes = gmdate('i', $date) * 60;
        $seconds = gmdate('s', $date);
        return ($hours + $minutes + $seconds);
    }

    // 依據code指向model 檢查通知是否已被寫入
    private function checkNotifyStatus($code, $moid)
    {
        return Notification::where('code', $code)
            ->where('mo_id', $moid)
            ->first() ? true : false;
    }

    // 待寫入資料集合
    public function dataCollection($code, $data, $mac_id = null)
    {
        $notiInfo = SetupNotification::where('code', $code)
                        ->first();
        $rfidData = MesRegister::with('relatedMo')
                        ->where('mo_id', $data->mo_id)
                        ->first();
        $moData = MesMo::where('mo_id', $data->mo_id)
                    ->first();
        return [
            'code' => $code,
            'infor_date' => date('Y-m-d'),
            'infor_time' => date('H:i:s'),
            'role_name' => $notiInfo->role_name,
            'mac_id' => !is_null($mac_id) ? $mac_id : $data->mac_id,
            'rfid_type' => $data->rfid_type,
            'company_id' => $rfidData->company_id,
            'factory_id' => $rfidData->factory_id,
            'rfid_status' => 3,
            'mo_id' => $data->mo_id,
            'item' => $data->item,
            'qty' => $rfidData->relatedMo->qty,
            'completion_date' => $rfidData->relatedMo->completion_date,
            'so_id' => $rfidData->relatedMo->so_id,
            'customer_id' => $rfidData->relatedMo->customer_id,
            'customer_name' => $rfidData->relatedMo->customer_name,
            'line_id' => $rfidData->relatedMo->line_id,
            'line_name' => $rfidData->relatedMo->line_name,
            'prod_qty' => $rfidData->relatedMo->prod_qty,
            'fin_qty' => $rfidData->relatedMo->fin_qty,
            'nf_qty' => $rfidData->relatedMo->nf_qty
        ];
    }

    // 檢查是否已強制列印
    public function checkForce($data)
    {
        return MesQcPassMo::where('mo_id', $data->mo_id)
            ->where('mac_id', $data->mac_id)
            ->where('rfid_type', 'B')
            ->where('force_print_time', '<>', null)
            ->orderBy('created_at', 'DESC')
            ->first();
    }

    // 將已通過且被強制列印過tag之列印時間移至其他未完工tag
    public function updatePrintTime($data)
    {
        $rfidEntities = [new MesRfid, new MesMoRfid];
        $qcpassEntities = [new MesQcPass, new MesQcPassMo];
        $forceMac = MesQcPassMo::where('mo_id', $data->mo_id)
                    ->where('mac_id', $data->mac_id)
                    ->where('rfid_type', 'B')
                    ->where('force_print_time', '<>', null)
                    ->orderBy('created_at', 'DESC')
                    ->first();
        $nextFirst = MesQcPassMo::where('mo_id', $data->mo_id)
                    ->where('rfid_type', 'B')
                    ->where('force_print_time', null)
                    ->where('end_datetime', null)
                    ->where('qc_pass', '')
                    ->first();
        if (!is_null($nextFirst)) {
            $nextFirst->update([
                'force_print_time' => $forceMac->force_print_time,
                'force_print_date' => $forceMac->force_print_date,
                'rfid_status' => 4,
                'qc' => $forceMac->qc,
                'qc_pass' => $forceMac->qc_pass,
                'printed' => 1
            ]);
            MesQcPass::where('mo_id', $data->mo_id)
                ->where('mac_id', $nextFirst->mac_id)
                ->where('qc_pass', '')
                ->update([
                    'force_print_time' => $forceMac->force_print_time,
                    'force_print_date' => $forceMac->force_print_date,
                    'rfid_status' => 4,
                    'qc' => $forceMac->qc,
                    'qc_pass' => $forceMac->qc_pass,
                    'printed' => 1
                ]);
            foreach ($rfidEntities as $key => $entity) {
                $entity->where('mo_id', $data->mo_id)
                    ->where('mac_id', $nextFirst->mac_id)
                    ->where('end_datetime', null)
                    ->where('qc_pass', '')
                    ->update([
                        'rfid_status' => 4,
                        'qc' => $forceMac->qc,
                        'qc_pass' => $forceMac->qc_pass
                    ]);
            }
        }
        
        // -------------------------------------
        foreach ($qcpassEntities as $key => $entity) {
            $entity->where('mo_id', $data->mo_id)
                ->where('mac_id', $data->mac_id)
                ->where('rfid_type', 'B')
                ->where('force_print_time', '<>', null)
                ->orderBy('created_at', 'DESC')
                ->first()
                ->update([
                    'force_print_date' => null,
                    'force_print_time' => null
                ]);
        }
    }
}