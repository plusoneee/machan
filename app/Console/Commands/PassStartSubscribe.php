<?php

namespace App\Console\Commands;

use DB;
use Log;
use Redis;
use Illuminate\Console\Command;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesMoRfid;
use App\Entities\MesRecord;
use App\Entities\MesQcPass;
use App\Entities\MesRegister;
use App\Entities\MesTimeflag;
use App\Entities\MesQcPassMo;
use App\Entities\AppSuspend;
use App\Entities\Notification;
use App\Entities\SetupNotification;

class PassStartSubscribe extends Command
{
    private $table = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channel:PassStartSubscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->table = [
            'mes_records' => [],
            'mes_rfids' => [],
            'mes_mo_rfids' => [],
            'mes_qc_passes' => [],
            'mes_qc_pass_mos' => [],
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::subscribe(['start-channel'], function($message) {
            if (!is_null($message) && !empty($message)) {
                try {
                    $data = json_decode($message);
                    switch ($data->rfid_type) {
                        case 'G':
                            $moInfo = MesMo::where('mo_id', $data->mo_id)
                                        ->first();
                            Log::info($data->mo_id.' - '.$data->mac_id.' pass start');

                            $table = $this->tableInitialize($data, $this->table);
                            $mergeData = $this->addRepeat($data, $table);
                            $this->storeData($data->mo_id, $data->mac_id, $mergeData);
                            
                            if ($moInfo->mo_status === 0) {  // 若製令狀態為0 才可更新開始時間 與 預計完成時間
                                $this->calcChangeTime($data);
                                $this->calcStartStatus($data, $moInfo);
                            }
                            
                            Log::info($data->mo_id.' - '.$data->mac_id.' pass start successfully');
                            $this->notifyPeople($data);
                            $this->notifyChangeTime($data);
                            break;
                        
                        case 'B':
                            // 累計開工數 + 1
                            $moInfo = MesMo::where('mo_id', $data->mo_id)
                                        ->first();
                            $prodQtySum = $moInfo->accumulator_start + 1;
                            MesMo::where('mo_id', $data->mo_id)
                                ->update(['accumulator_start' => $prodQtySum]);

                            Log::info($data->mo_id.' - '.$data->mac_id.' pass start');
                            if ($moInfo->mo_status === 0) {
                                $this->calcChangeTime($data);
                                $this->calcStartStatus($data, $moInfo);  // 無Green開始或先讀到Blue 寫入換模結束時間與預計完成時間
                            }

                            $table = $this->tableInitialize($data, $this->table);
                            $mergeData = $this->addRepeat($data, $table);
                            $this->storeData($data->mo_id, $data->mac_id, $mergeData);

                            Log::info($data->mo_id.' - '.$data->mac_id.' pass start successfully');
                            $this->notifyPeople($data);
                            $this->notifyChangeTime($data);
                            $this->notifyLastOne($data, 'B');
                            $this->notifyNonGreen($data);
                            break;
                        
                        case 'R':
                            Log::info($data->mo_id.' - '.$data->mac_id.' pass start');
                            $table = $this->tableInitialize($data, $this->table);
                            $mergeData = $this->addRepeat($data, $table);
                            $this->storeData($data->mo_id, $data->mac_id, $mergeData);
                            Log::info($data->mo_id.' - '.$data->mac_id.' pass start successfully');
                            $this->notifyLastOne($data, 'R');
                            break;
                    }
                } catch (\Exception $e) {
                    echo date('Y-m-d H:i:s');
                    echo $e;
                }
            }
        });
    }

    // table 屬性初始化 定義value
    private function tableInitialize($data, $table)
    {
        $table['mes_registers'] = [
            'rfid_status' => 3
        ];
        $table['mes_rfids'] = [
            'start_profile' => $data->profile,
            'start_datetime' => $data->start_datetime,
            'rfid_status' => 3,
        ];
        return $table;
    }

    // 新增重複欄位
    public function addRepeat($data, $table)
    {
        $recordArray = ['rfid_status' => 3];
        $table['mes_records'] = array_merge($table['mes_records'], $recordArray);

        $recordArray = [
            'start_line' => $data->line_id,
            'start_antenna_id' => $data->antenna_id,
        ];
        $table['mes_rfids'] = array_merge($table['mes_rfids'], $recordArray);

        $recordArray = [
            'start_datetime' => $data->start_datetime,
            'rfid_status' => 3,
            'mo_status' => 1,
            'start_line' => $data->line_id,
            'start_antenna_id' => $data->antenna_id,
            'start_profile' => $data->profile,
        ];
        $table['mes_mo_rfids'] = array_merge($table['mes_mo_rfids'], $recordArray);

        $recordArray = [
            'rfid_status' => 3,
            'mo_status' => 1,
        ];
        $table['mes_qc_passes'] = array_merge($table['mes_qc_passes'], $recordArray);

        $recordArray = [
            'rfid_status' => 3,
            'date' => $data->start_date,
            'mo_status' => 1,
            'line_id' => $data->line_id,
            'line_name' => $data->line_name,
            'routing' => $data->routing,
            'profile' => $data->profile,
        ];
        $table['mes_qc_pass_mos'] = array_merge($table['mes_qc_pass_mos'], $recordArray);
        return $table;
    }

    // 將資料寫入MySQL
    public function storeData($mo_id, $mac_id, $mergeData)
    {
        // 以迴圈方式 針對model進行更新
        // entities 與 tables 呼應相同資料表
        $entities = [new MesRegister, new MesRecord, new MesRfid, new MesMoRfid, new MesQcPass, new MesQcPassMo];
        $tables = ['mes_registers', 'mes_records', 'mes_rfids', 'mes_mo_rfids', 'mes_qc_passes', 'mes_qc_pass_mos'];
        foreach ($entities as $key => $entity) {
            $entity->where('mac_id', $mac_id)
                ->where('mo_id', $mo_id)
                ->orderBy('created_at', 'DESC')
                ->first()
                ->update($mergeData[$tables[$key]]); // 由於entities 與 tables 兩陣列順序相同，所以可直接使用foreach key變數
        }
    }

    // 通過Start 計算當批換模換線結束
    private function calcChangeTime($data)
    {
        $previous = MesTimeflag::where('profile', $data->profile)
                        ->whereDate('date', date('Y-m-d'))
                        ->where('type', 2)
                        ->orderBy('created_at', 'desc')
                        ->first();
        $autoTime = MesTimeflag::where('profile', $data->profile)
                        ->whereDate('date', date('Y-m-d'))
                        ->where('type', 0)
                        ->first();
        $endRecord = MesTimeflag::where('profile', $data->profile)
                        ->whereDate('date', date('Y-m-d'))
                        ->where('cl_end_time', null)
                        ->orderBy('created_at', 'desc')
                        ->first();
        // 當批製令換模結束
        if ($endRecord) {
            $changeStart = !is_null($previous) ? $previous->cl_start_time : $autoTime->time;
            $changeTime = strtotime(date('H:i:s')) - strtotime($changeStart);
            MesTimeflag::create([
                'profile' => $data->profile,
                'type' => 1,
                'date' => date('Y-m-d'),
                'cl_end_time' => date('H:i:s'),
                'mo_id' => $data->mo_id,
                'change_time' => gmdate('H:i:s', $changeTime),
            ]);
            MesMo::where('mo_id', $data->mo_id)
                ->update([
                    'change_date' => date('Y-m-d'),
                    'change_time' => gmdate('H:i:s', $changeTime),
                    'change_line_id' => $data->line_id,
                    'change_line_name' => $data->line_name,
                    'change_profile' => $data->profile
                ]);

            // 換模換線超過10分鐘 發送除外工時 - 換模換線通知
            if ($changeTime > strtotime('00:10:00')) {
                $this->notifySuspend($data);
            }
        }
    } 

    // 開始區 寫入預計完成時間與製令換模結束時間
    public function calcStartStatus($data, $moInfo)
    {
        MesMo::where('mo_id', $data->mo_id)
            ->update([
                'mo_status' => 1,
                'date' => $data->start_date,
                'line_id' => $data->line_id,
                'line_name' => $data->line_name,
                'profile' => $data->profile,
                'start_time' => $data->start_time,
                'first_start_time' => $data->start_time,
                'f_completion_time' => date('H:i:s', strtotime(date('Y-m-d H:i:s')) + ($moInfo->tht * $moInfo->qty / 18))
            ]);
        MesMoRfid::where('mo_id', $data->mo_id)
            ->update(['cl_end_time' => date('H:i:s')]);
    }

    // 開線人數通知
    public function notifyPeople($data)
    {
        $code = 'N001'; // 開線人數通知代號
        $checkStatus = $this->checkNotifyStatus($code, $data->mo_id);
        if ($checkStatus === false) {
            $dataTmp = $this->dataCollection($code, $data);
            Notification::create($dataTmp);
        }
    }

    // 換模換線通知
    public function notifyChangeTime($data)
    {
        $code = 'N002'; // 換模換線通知代號
        $checkStatus = $this->checkNotifyStatus($code, $data->mo_id);
        if ($checkStatus === false) {
            $dataTmp = $this->dataCollection($code, $data);
            Notification::create($dataTmp);
        }
    }

    // 開始時間異常通知 (無Green Tag)
    public function notifyNonGreen($data)
    {
        $code = 'N003'; // 無Green Tag開始通知代號
        $moData = MesMo::where('mo_id', $data->mo_id)
                    ->first();
        $checkStatus = $this->checkNotifyStatus($code, $data->mo_id);
        $checkGreen = $this->getGreen($data->mo_id);
        if ($moData->accumulator_start >= 2) {
            if ($checkGreen === 0) {
                if ($checkStatus === false) {
                    $dataTmp = $this->dataCollection($code, $data);
                    Notification::create($dataTmp);
                }
            }
        }

    }

    // 取得該製令Green Tag資訊
    private function getGreen($moid)
    {
        return MesMoRfid::where('mo_id', $moid)
            ->where('rfid_type', 'G')
            ->where('rfid_status', 3)
            ->first() ? 1 : 0;
    }

    // 最後一筆通知
    public function notifyLastOne($data, $color)
    {
        $code = 'N007'; // 最後一筆通知代號
        $moData = MesMo::where('mo_id', $data->mo_id)
                    ->first();
        $checkStatus = $this->checkNotifyStatus($code, $data->mo_id);
        switch ($color) {
            case 'B':
                if ($moData->accumulator_start === $moData->qty) {   // 投入 = 訂單數量 觸發
                    if ($checkStatus === false) {
                        $dataTmp = $this->dataCollection($code, $data);
                        Notification::create($dataTmp);
                    }
                }
                break;
            
            case 'R':   // Red TAG觸發
                if ($checkStatus === false) {
                    $dataTmp = $this->dataCollection($code, $data);
                    Notification::create($dataTmp);
                }
                break;
        }
    }

    // 寫入除外工時 - 換模換線通知
    public function notifySuspend($data)
    {
        $code = 'N018';
        $dataTmp = [];
        if ($this->checkNotifyStatus($data->mo_id) === false) {
            $dataTmp = $this->dataCollection($code, $data);
            Notification::create($dataTmp);
            AppSuspend::create($dataTmp);
        }
    }

    // 檢查當批製令 換模換線時間 是否超過10分鐘
    public function checkChangeTime($mo_id)
    {
        $timeTmp = strtotime('00:10:00');
        $moInfo = MesMo::where('mo_id', $mo_id)
                    ->first();
        $changeTime = strtotime($moInfo->change_time);
        if ($changeTime >= $timeTmp) {
            return true;    // 大於10分鐘 回傳true
        }
    }

    // 依據code指向model 檢查通知是否已被寫入
    private function checkNotifyStatus($code, $moid)
    {
        return Notification::where('mo_id', $moid)
            ->where('code', $code)
            ->first() ? true : false;
    }

    // 待寫入資料集合
    public function dataCollection($code, $data)
    {
        $notiInfo = SetupNotification::where('code', $code)
                        ->first();
        $rfidData = MesRegister::with('relatedMo')
                        ->where('mo_id', $data->mo_id)
                        ->first();
        $moData = MesMo::where('mo_id', $data->mo_id)
                    ->first();
        return [
            'code' => $code,
            'infor_date' => $data->start_date,
            'infor_time' => $data->start_time,
            'role_name' => $notiInfo->role_name,
            'mac_id' => $data->mac_id,
            'rfid_type' => $data->rfid_type,
            'company_id' => $rfidData->company_id,
            'factory_id' => $rfidData->factory_id,
            'rfid_status' => 3,
            'mo_id' => $data->mo_id,
            'item' => $data->item,
            'qty' => $rfidData->relatedMo->qty,
            'completion_date' => $rfidData->relatedMo->completion_date,
            'so_id' => $rfidData->relatedMo->so_id,
            'customer_id' => $rfidData->relatedMo->customer_id,
            'customer_name' => $rfidData->relatedMo->customer_name,
            'line_id' => $rfidData->relatedMo->line_id,
            'line_name' => $rfidData->relatedMo->line_name
        ];
    }
}
