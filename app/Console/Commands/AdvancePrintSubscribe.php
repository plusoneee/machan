<?php

namespace App\Console\Commands;

use DB;
use Redis;
use App\Entities\MesMo;
use App\Entities\MesRfid;
use App\Entities\MesRecord;
use App\Entities\MesQcPass;
use App\Entities\MesMoRfid;
use App\Entities\MesQcPassMo;
use App\Entities\MesRegister;
use App\Entities\Notification;
use App\Entities\SetupNotification;
use Illuminate\Console\Command;

class AdvancePrintSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channel:AdvancePrintSubscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::subscribe(['advance-channel'], function($message) {
            if (!is_null($message) && !empty($message)) {
                try {
                    $data = json_decode($message);
                    $rfidEntities = [new MesRfid, new MesMoRfid];
                    $mappingEntities = [new MesRegister, new MesRecord];
                    $qcpassEntities = [new MesQcPass, new MesQcPassMo];

                    // 強制列印
                    if ($data->state === 3) {
                        $first = MesQcPassMo::where('mo_id', $data->mo_id)
                                    ->where('qc_pass', '')
                                    ->where('rfid_type', 'B')
                                    ->where('rfid_status', 3)
                                    ->where('force_print_date', null)
                                    ->first();
                                // ->where('qc_pass', '')
                                // ->where('rfid_type', 'B')
                                // ->where('force_print_date', '')
                        if ($first) {
                            // if ($first->rfid_status === 1) {
                            //     $moData = MesMo::where('mo_id', $data->mo_id)
                            //                 ->first();
                            //     MesMo::where('mo_id', $data->mo_id)
                            //         ->update(['accumulator_start' => $moData->accumulator_start + 1]);
                            // }
                            
                            $checkFin = MesQcPassMo::where('mo_id', $data->mo_id)
                                    ->where('rfid_status', 4)
                                    ->where('rfid_type', 'B')
                                    ->count();
                            
                            $newQcpass = $data->mo_id.'_'.($checkFin + 1);

                            $first->update([
                                'end_line' => $data->line_id,
                                'end_profile' => $data->profile,
                                'end_datetime' => date('Y-m-d H:i:s'),
                                'rfid_status' => 4,
                                'qc' => $data->qc,
                                'force_print_date' => date('Y-m-d'),
                                'force_print_time' => date('H:i:s'),
                                'printed' => 1,
                                'qc_pass' => $newQcpass
                            ]);

                            MesQcPass::where('mo_id', $data->mo_id)
                                ->where('mac_id', $first->mac_id)
                                ->where('qc_pass', '')
                                ->update([
                                    'rfid_status' => 4,
                                    'qc' => $data->qc,
                                    'force_print_date' => date('Y-m-d'),
                                    'force_print_time' => date('H:i:s'),
                                    'printed' => 1,
                                    'qc_pass' => $newQcpass
                                ]);
                            
                            foreach ($rfidEntities as $key => $entity) {
                                $entity->where('mo_id', $data->mo_id)
                                    ->where('mac_id', $first->mac_id)
                                    ->where('end_datetime', null)
                                    ->where('qc_pass', '')
                                    ->update([
                                        'end_line' => $data->line_id,
                                        'end_profile' => $data->profile,
                                        'end_datetime' => date('Y-m-d H:i:s'),
                                        'rfid_status' => 4,
                                        'qc' => $data->qc,
                                        'qc_pass' => $newQcpass
                                    ]);
                            }

                            foreach ($mappingEntities as $key => $entity) {
                                $entity->where('mo_id', $data->mo_id)
                                    ->where('mac_id', $first->mac_id)
                                    ->update(['rfid_status' => 4]);
                            }

                            $this->updateFinishQty($data);
                            $this->notifyForcePrint($data, $newQcpass, $first->mac_id);
                        }
                    } else {
                        // 將預先列印之tag狀態改為已完工
                        foreach ($qcpassEntities as $key => $entity) {
                            $entity->where('mac_id', $data->mac_id)
                                ->where('mo_id', $data->mo_id)
                                ->orderBy('created_at', 'DESC')
                                ->first()
                                ->update([
                                    'qc_pass' => $data->qc_pass,
                                    'print_date' => date('Y-m-d'),
                                    'print_time' => date('H:i:s'),
                                    'rfid_status' => 4,
                                    'qc' => $data->qc,
                                    'printed' => 1
                                ]);
                        }

                        foreach ($rfidEntities as $key => $entity) {
                            $entity->where('mac_id', $data->mac_id)
                                ->where('mo_id', $data->mo_id)
                                ->orderBy('created_at', 'DESC')
                                ->first()
                                ->update([
                                    'rfid_status' => 4,
                                    'qc' => $data->qc,
                                    'qc_pass' => $data->qc_pass
                                ]);
                        }

                        foreach ($mappingEntities as $key => $entity) {
                            $entity->where('mo_id', $data->mo_id)
                                ->where('mac_id', $data->mac_id)
                                ->update(['rfid_status' => 4]);
                        }
                        $this->rewriteStatus($data);
                        $this->updateFinishQty($data);
                    }
                } catch (\Exception $e) {
                    echo date('Y-m-d H:i:s');
                    echo $e;
                }
            }
        });
    }

    public function updateFinishQty($data)
    {
        $finQty = MesQcPassMo::where('mo_id', $data->mo_id)
                    ->where('rfid_type', 'B')
                    ->where('rfid_status', 4)
                    ->where('printed', 1)
                    ->count();
        $moData = MesMo::where('mo_id', $data->mo_id)
                    ->first();
        $accuStart = $moData->accumulator_start;
        $blueNf = $moData->nf_qty;
        $passRate = $accuStart != 0 ? round((($finQty - $blueNf) / $accuStart), 2) * 100 . '%' : '0%';
        $moData->update([
            'accumulator_fin' => $finQty,
            'first_pass_rate' => $passRate
        ]);
    }

    // 複寫紅 綠TAG狀態
    public function rewriteStatus($data)
    {
        $tables = ['mes_qc_passes', 'mes_qc_pass_mos', 'mes_mo_rfids', 'mes_rfids', 'mes_records', 'mes_registers'];
        foreach ($tables as $key => $table) {
            DB::table($table)
                ->where('rfid_type', 'R')
                ->orWhere('rfid_type', 'G')
                ->update(['rfid_status' => 4]);
        }
    }

    // N016 強制列印通知
    public function notifyForcePrint($data, $qcpass, $macid)
    {
        $code = 'N016';
        $dataTmp = $this->dataCollection($code, $data, $macid);
        Notification::create($dataTmp);
    }

    // 待寫入資料集合
    private function dataCollection($code, $data, $mac_id = null)
    {
        $notiInfo = SetupNotification::where('code', $code)
                        ->first();
        $rfidData = MesRegister::with('relatedMo')
                        ->where('mo_id', $data->mo_id)
                        ->first();
        $moData = MesMo::where('mo_id', $data->mo_id)
                    ->first();
        return [
            'code' => $code,
            'infor_date' => date('Y-m-d'),
            'infor_time' => date('H:i:s'),
            'role_name' => $notiInfo->role_name,
            'mac_id' => !is_null($mac_id) ? $mac_id : $data->mac_id,
            'company_id' => $rfidData->company_id,
            'factory_id' => $rfidData->factory_id,
            'mo_id' => $data->mo_id,
            'qty' => $rfidData->relatedMo->qty,
            'completion_date' => $rfidData->relatedMo->completion_date,
            'so_id' => $rfidData->relatedMo->so_id,
            'customer_id' => $rfidData->relatedMo->customer_id,
            'customer_name' => $rfidData->relatedMo->customer_name,
            'line_id' => $rfidData->relatedMo->line_id,
            'line_name' => $rfidData->relatedMo->line_name,
            'prod_qty' => $rfidData->relatedMo->prod_qty,
            'fin_qty' => $rfidData->relatedMo->fin_qty,
            'nf_qty' => $rfidData->relatedMo->nf_qty
        ];
    }
}
