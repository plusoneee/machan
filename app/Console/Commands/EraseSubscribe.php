<?php

namespace App\Console\Commands;

use DB;
use Redis;
use App\Entities\MesMo;
use App\Entities\MesRegister;
use Illuminate\Console\Command;

class EraseSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channel:EraseSubscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::subscribe(['erase-channel'], function($message) {
            if (!is_null($message) && !empty($message)) {
                try {
                    $data = json_decode($message);
                    $infos = $data->mac_id; // 欲消除之TAG            
                    $this->deleteData($infos);  // 清除未消除之TAG
                    MesRegister::whereIn('mac_id', $infos)
                        ->update([
                            'mo_id' => '',
                            'record_date' => null,
                            'rfid_status' => 2,
                            'erase_date' => date('Y-m-d')
                        ]);
                } catch (\Exception $e) {
                    echo date('Y-m-d H:i:s');
                    echo $e;
                }
            }
        });
    }

    // 強制消除 清空未上線RFID資訊 (rfid_status 為 1)
    private function deleteData($mac_info)
    {
        $tables = ['mes_records', 'mes_rfids', 'mes_mo_rfids', 'mes_qc_pass_mos', 'mes_qc_passes'];
        foreach ($tables as $key => $table) {
            DB::table($table)
                ->whereIn('mac_id', $mac_info)
                ->where(function ($query) {
                    $query->where('rfid_status', 1)
                        ->orWhere('rfid_status', 3);
                })->delete();
        }
    }
}
