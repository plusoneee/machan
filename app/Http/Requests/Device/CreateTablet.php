<?php

namespace App\Http\Requests\Device;

use Illuminate\Foundation\Http\FormRequest;

class CreateTablet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tablet_id' => 'required|unique:setup_tablets,tablet_id',
            'mac_id' => 'required',
            'role' => 'required',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'tablet_id.required' => '請輸入Tablet ID',
            'tablet_id.unique' => 'Tablet代號已重複',
            'mac_id.required' => '請輸入平板MAC',
            'role.required' => '請輸入角色名稱',
        ];
    }
}
