<?php

namespace App\Http\Requests\Setup;

use Illuminate\Foundation\Http\FormRequest;

class CreateUnfinished extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nf_code' => 'required|unique:setup_non_finisheds,nf_code',
            'exception' => 'required',
        ];
    }

    /** 
     * Get the validation messages that apply to the request
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'nf_code.required' => 'please enter your nf code',
            'nf_code.unique' => '尾數碼已重複',
            'exception.required' => 'please enter your exception',
        ];
    }
}
