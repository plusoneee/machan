<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\NotifyListRepository;
use App\Services\GetProduceOrder;

class NotifyListController extends Controller
{
    protected $listRepo = null;
    protected $produceService = null;

    public function __construct(NotifyListRepository $listRepo, GetProduceOrder $produceService)
    {
        $this->listRepo = $listRepo;
        $this->produceService = $produceService;
    }

    public function index($code)
    {
        $listInfo = $this->listRepo->index($code, auth()->user());
        $listInfo = $this->addItem($listInfo);
        return response()->json([
            'status' => 0,
            'data' => $listInfo
        ]);
    }

    // 新增物料名稱至array
    public function addItem($datas)
    {
        if (!$datas) {
            return [];
        }
        foreach ($datas as $key => $data) {
            if ($data['type'] === 'N001' || $data['type'] === 'N002' || $data['type'] === 'N007' || $data['type'] === 'N009' || $data['type'] === 'N003' || $data['type'] === 'N011' || $data['type'] === 'N017' || $data['type'] === 'N016') {
                $material = $this->produceService->getBillNoName($data['item']);
                $itemName = $this->checkMaterial($material);
                $data['item_name'] = $itemName;
                $datas[$key] = $data;
            }
        }
        return $datas;
    }

    private function checkMaterial($material)
    {
        if ($material) {
            try {
                return $material->MaterialName;
            } catch (\Exception $e) {
                return '取得料名失敗，請檢查Web Service服務狀態';
            }
        } 
        return '(無法取得料名)';
    }
}
