<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Inquire\UnfinishedReason;
use App\Http\Requests\Inquire\MoIdListRequest;
use App\Repositories\InquireRepository;
use App\Services\GetProduceOrder;

class InquireController extends Controller
{
    protected $inquireRepo = null;
    protected $produceService = null;

    public function __construct(InquireRepository $inquireRepo, GetProduceOrder $produceService)
    {
        $this->inquireRepo = $inquireRepo;
        $this->produceService = $produceService;
    }
    
    /**
     * RFID TAG 數據
     */
    public function getRfidInfo($mac_id)
    {
        $data = $this->inquireRepo->rfidInfo($mac_id);
        $newData = $this->getMaterial($data[1]);
        for ($i = 0; $i < count($data[0]); $i++) {
            $Material = $this->produceService->getBillNoName($data[0][0]['item']);
            $data[0][$i]['item_name'] = $Material ? $Material->MaterialName : '(無法取得料名)';
        }
    	return response()->json([
    		'status' => 0,
            'message' => 'success',
            'tag_id' => $data[2],
    		'newest' => $newData,
            'history' => $data[0]
    	]);
    }

    /**
     * 取得料名
     */
    private function getMaterial($data)
    {
        if (empty($data['item'])) {
            return $data;
        }
        $Material = $this->produceService->getBillNoName($data['item']);
        $data['item_name'] = $Material ? $Material->MaterialName : '(無法取得料名)';
        return $data;
    }

    /**
     * 製令報工數據 (明細)
     */
    public function getMoInfo($mo_id)
    {
    	$data = $this->inquireRepo->moInfo($mo_id);
    	return response()->json([
    		'status' => 0,
    		'message' => 'success',
    		'data' => $data
    	]);
    }

    /**
     * 製令報工數據 (清單)
     */
    public function getMoList(MoIdListRequest $request)
    {
    	$data = $this->inquireRepo->moList(request()->all());
    	return response()->json([
    		'status' => 0,
    		'message' => 'success',
    		'data' => $data
    	]);
    }

    /**
     * QC PASS 模組
     */
    public function getQcpassInfo()
    {
        $data = $this->inquireRepo->qcpassInfo(request()->all());
        if ($data === 1) {
            return response()->json(['status' => 1, 'message' => 'QC PASS or mo not found'], 404);
        }
        
        $Material = $this->produceService->getBillNoName($data[0]['item']);
        $data[0]['item_name'] = $Material ? $Material->MaterialName : '(無法取得料名)';
        switch (count($data)) {
            case 1:       
                return response()->json([
                    'status' => 0,
                    'message' => 'success',
                    'data' => $data[0],
                ]);
            
            case 2:
                return response()->json([
                    'status' => 0,
                    'message' => 'success',
                    'data' => $data[0],
                    'qc_pass' => $data[1]
                ]);
        }
    }

    /**
     * 強制列印清單
     */
    public function getQcpassForceList()
    {
        $data = $this->inquireRepo->forceList();
    	return response()->json([
    		'status' => 0,
    		'message' => 'success',
    		'data' => $data
    	]);
    }
    
    /**
     * 強制列印明細
     */
    public function getQcpassForceInfo($mo_id)
    {
    	$nonPrints = $this->inquireRepo->forceInfo($mo_id);
        $billNoName = '';
        if (!empty($nonPrints['item'])) {
            $billNoName = $this->produceService->getBillNoName($nonPrints['item']);
        }
    	return response()->json([
    		'status' => 0,
    		'message' => 'success',
    		'data' => $nonPrints['data'],
            'customer_name' => $nonPrints['customer_name'],
            'accumulator_start'=> $nonPrints['accumulator_start'],
            'accumulator_fin'=> $nonPrints['accumulator_fin'],
            'item_name' => $billNoName ? $billNoName->MaterialName : '(無法取得料名)',
            'qty' => $nonPrints['qty']
    	]);
    }
    
    /**
     * 預先列印明細
     */
    public function getQcpassAdvancePrint($mo_id)
    {
        $item_name = '';
        $data = $this->inquireRepo->advancePrint($mo_id);
        if (count($data) > 0) {
            $Material = $this->produceService->getBillNoName($data->first()->item);
            $item_name = $Material ? $Material->MaterialName : '(無法取得料名)';
        }

    	return response()->json([
    		'status' => 0,
    		'message' => 'success',
    		'data' => $data,
            'item_name' => $item_name,
            'qty' => count($data) > 0 ? $data->first()->qty : 0,
            'customer_name' => count($data) > 0 ? $data->first()->customer_name : ''
    	]);
    }

    /**
     * 取得在線製令
     */
    public function getOnlineMo()
    {
        $data = $this->inquireRepo->onlineMo();
        return response()->json([
            'status' => 0,
            'message' => 'success',
            'data' => $data
        ]);
    }

    /**
     * 取得欲消除之製令資訊
     */
    public function getEraseMo($mo_id)
    {
        $moData = $this->inquireRepo->eraseMo($mo_id);
        return response()->json([
            'status' => 0,
            'message' => 'success',
            'mo_id' => $moData['mo_id'],
            'customer_name' => $moData['customer_name'],
            'qty' => $moData['qty']
        ]);
    }

    /**
     * 回傳尾數異常原因
     */
    public function reasonIndex()
    {
        return response()->json(['status' => 0, 'reasons' => $this->inquireRepo->getReasonIndex()]);
    }

    /**
     * 新增尾數異常原因
     */
    public function createReason(UnfinishedReason $request)
    {
        $ngInfo = $this->inquireRepo->storeReason(request()->exception);
        return response()->json(['status' => 0, 'ngInfo' => $ngInfo]);
    }

    /**
     * 回傳除外工時原因
     */
    public function exceptionIndex()
    {
        return response()->json(['status' => 0, 'exceptions' => $this->inquireRepo->getExceptionIndex()]);
    }
}
