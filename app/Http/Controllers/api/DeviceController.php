<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\DeviceRepository;
use App\Services\NodeReaderService;

class DeviceController extends Controller
{
    private $deviceRepo;
    private $iotService;

    public function __construct(DeviceRepository $deviceRepo, NodeReaderService $iotService)
    {
        $this->deviceRepo = $deviceRepo;
        $this->iotService = $iotService;
    }

    /**
     * 上班按鈕 新增當日換模換線開始
     */
    public function workOn()
    {
        $user = auth()->user();
        $readerInfo = $user->profileScope($user->line_id)->first();  // User 關聯 SetupReader 進行 Scope
        
    	$resStart = $this->iotService->runStartReader($readerInfo->reader_id, $user->line_id);
    	$resEnd = $this->iotService->runEndReader($readerInfo->reader_id, $user->line_id);
        
    	if ($resStart === 1 || $resEnd === 1) {
            return response()->json(['status' => 1, 'message' => 'run error']);            
        }

        $response = $this->deviceRepo->createChangeTime($user);
        if (!$response) {
            return response()->json(['status' => 1, 'message' => 'change time create fail']);
        }

    	return response()->json(['status' => 0]);
    }

    /**
     * 下班按鈕 並儲存當日未完工RFID TAG
     */
    public function workOff()
    {
        // 若有傳入line_id 表示該人員為不綁定線別
        $user = auth()->user();
    	$resStart = $this->iotService->stopStartReader($user->line_id);
        $resEnd = $this->iotService->stopEndReader($user->line_id);
        if ($resStart === 1 || $resEnd === 1) {
            return response()->json(['status' => 1, 'message' => 'reader shutdown error']);
        }

        $response = $this->deviceRepo->postOffWork($user);
        if ($response !== true) {
            return response()->json(['status' => 1, 'message' => 'data write back error']);
        }

        return response()->json([
            'status' => 0,
            'off_time' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * Reader 重啟按鈕
     */
    public function restartReader()
    {
        $user = auth()->user();
        $readerInfo = $user->profileScope($user->line_id)->first();

        $resStart = $this->iotService->runStartReader($readerInfo->reader_id, $user->line_id);
        $resEnd = $this->iotService->runEndReader($readerInfo->reader_id, $user->line_id);
        
        if ($resStart === 1 || $resEnd === 1) {
            return response()->json(['status' => 1, 'message' => 'run error']);
        } 

        return response()->json(['status' => 0]);
    }
}
