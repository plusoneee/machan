<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    protected $userRepo = null;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
        // $this->middleware('jwt.auth');
    }

    public function show()
    {
        return response()->json(['status' => 0, 'user' => $this->userRepo->find()]);
    }
}
