<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Notification\InputPeople;
use App\Http\Requests\Notification\InputMoStart;
use App\Http\Requests\Notification\InputProStart;
use App\Http\Requests\Notification\InputUnfinished;
use App\Http\Requests\Notification\InputSuspend;
use App\Repositories\NotifyRepository;
use App\Services\GetProduceOrder;

class NotifyController extends Controller
{
    private $notifyRepo;
    private $produceService;

    public function __construct(NotifyRepository $notifyRepo, GetProduceOrder $produceService)
    {
        $this->notifyRepo = $notifyRepo;
        $this->produceService = $produceService;
    }

    /**
     * 通知總表
     */
    public function index()
    {
        $user = auth()->user();
        $notiIndex = $this->notifyRepo->listIndex($user);
        $data = $this->addItem($notiIndex);
        return response()->json(['status' => 0, 'data' => $data]);
    }

    /**
     * 新增物料名稱
     */
    public function addItem($datas)
    {
        if (!$datas) {
            return [];
        }
        foreach ($datas as $key => $data) {
            if ($data['type'] === 'N001' || $data['type'] === 'N002' || $data['type'] === 'N007' || $data['type'] === 'N009' || $data['type'] === 'N003' || $data['type'] === 'N011' || $data['type'] === 'N017' || $data['type'] === 'N016') {
                $material = $this->produceService->getBillNoName($data['item']);
                $itemName = $this->checkMaterial($material);
                $data['item_name'] = $itemName;
                $datas[$key] = $data;
            }
        }
        return $datas;
    }

    /**
     * 檢查物料API 連線狀態
     */
    private function checkMaterial($material)
    {
        if ($material) {
            try {
                return $material->MaterialName;
            } catch (\Exception $e) {
                return '取得料名失敗，請檢查Web Service服務狀態';
            }
        } 
        return '(無法取得料名)';
    }

    /**
     * 輸入開線人數
     */
    public function inputPeople(InputPeople $request, $id)
    {
        $data = $this->notifyRepo->postPeople($id, request()->actual_human);
        if ($data === 1) {
            return response()->json(['status' => 1, 'message' => 'notification not found'], 404);
        }
        if ($data === 2) {
            return response()->json(['status' => 1, 'message' => 'mo id not found'], 404);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 輸入製令開始時間
     */
    public function inputMoStart(InputMoStart $request, $id)
    {
        $data = $this->notifyRepo->postMoTime($id, request()->start_time);
        if ($data === 1) {
            return response()->json(['status' => 1, 'message' => 'notification not found'], 404);
        }
        if ($data === 2) {
            return response()->json(['status' => 1, 'message' => 'mo id not found'], 404);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 輸入成品開始時間
     */
    public function inputProStart(InputProStart $request, $id)
    {
        $data = $this->notifyRepo->postProStart($id, request()->start_time);
        if ($data === 1) {
            return response()->json(['status' => 1, 'message' => 'notification not found'], 404);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 輸入尾數異常原因
     */
    public function inputUnfinished(InputUnfinished $request, $id)
    {
        $data = $this->notifyRepo->postUnfinished($id, request()->all());
        if ($data === 1) {
            return response()->json(['status' => 1, 'message' => 'notification not found'], 404);
        }
        if ($data === 2) {
            return response()->json(['status' => 1, 'message' => 'unfinished rfids not found'], 404);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 輸入除外工時 (換模換線、作業效率異常)
     */
    public function inputSuspend(InputSuspend $request, $id)
    {
        $data = $this->notifyRepo->postSuspend($id, request()->all());
        if ($data === 1) {
            return response()->json(['status' => 1, 'message' => 'notification not found'], 404);
        }
        if ($data === 2) {
            return response()->json(['status' => 1, 'message' => 'mo suspend not found'], 404);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 回覆通知 (已讀型通知)
     */
    public function reply($id)
    {
        $data = $this->notifyRepo->replyNotification($id);
        if ($data === false) {
            return response()->json(['status' => 1, 'message' => 'notification not found'], 404);
        }
        return response()->json(['status' => 0]);
    }
}
