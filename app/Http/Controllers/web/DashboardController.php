<?php

namespace App\Http\Controllers\web;

use App\Repositories\DashboardRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    protected $boardRepo = null;

    public function __construct(DashboardRepository $boardRepo)
    {
        $this->boardRepo = $boardRepo;
    }

    public function showBoard()
    {
        return view('dashboard');
    }

    public function index($profile)
    {
    	$rfidInfo = $this->boardRepo->boardIndex($profile);
        return response()->json(['rfidInfo' => $rfidInfo]);
    }

    public function getAccumulateData($profile, $date)
    {
        $params = [
            'profile' => $profile,
            'date' => $date,
        ];
        $accuInfo = $this->boardRepo->accumulateData($params);
        return response()->json(['accuInfo' => $accuInfo]);
    }
}
