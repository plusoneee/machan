<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Device\CreateBoard;
use App\Http\Requests\Device\CreatePrint;
use App\Http\Requests\Device\CreateTablet;
use App\Http\Requests\Device\UpdateBoard;
use App\Http\Requests\Device\UpdatePrint;
use App\Http\Requests\Device\UpdateTablet;
use App\Repositories\HardwareRepository;

class HardwareController extends Controller
{
    protected $deviceRepo;

    public function __construct(HardwareRepository $deviceRepo)
    {
        $this->deviceRepo = $deviceRepo;
    }

    public function showTablets()
    {  
        $tablets = $this->deviceRepo->getTablets(request()->search);
        return view('device.tablet', ['tablets' => $tablets]);
    }

    public function showBoards()
    {
        $boards = $this->deviceRepo->getBoards(request()->search);
        return view('device.board', ['boards' => $boards]);
    }

    public function showPrints()
    {
        $prints = $this->deviceRepo->getPrints(request()->search);
        return view('device.print', ['prints' => $prints]);
    }

    public function showAntennas()
    {
        $antennas = $this->deviceRepo->getAntennas();
        return view('device.antenna', ['antennas' => $antennas]);
    }

    /**
     * SetupTablet
     */
    public function storeTablet(CreateTablet $request)
    {
        $data = request()->all();
        $tablet = $this->deviceRepo->createTablet($data);
        if (!$tablet) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    public function updateTablet(UpdateTablet $request, $id)
    {
        $param = request()->all();
        $result = $this->deviceRepo->updateTablet($param, $id);
        if ($result === false) {
            return response()->json(['status' => 1], 404);
        }
        return response()->json(['status' => 0]);
    }

    public function destroyTablet($id)
    {
        $result = $this->deviceRepo->deleteTablet($id);
        if ($result === false) {
            return response()->json(['status' => 1], 404);
        }
        return response()->json(['status' => 0]);        
    }

    /**
     * SetupEboard
     */
    public function storeBoard(CreateBoard $request)
    {
        $data = $this->deviceRepo->createBoard(request()->all());
        if (!$data) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    public function updateBoard(UpdateBoard $request, $id)
    {
        $result = $this->deviceRepo->updateBoard(request()->all(), $id);
        if ($result === false) {
            return response()->json(['status' => 1], 404);
        }
        return response()->json(['status' => 0]);
    }

    public function destroyBoard($id)
    {
        $result = $this->deviceRepo->deleteBoard($id);
        if ($result === false) {
            return response()->json(['status' => 1], 404);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * SetupPrint
     */
    public function storePrint(CreatePrint $request)
    {
        $data = $this->deviceRepo->createPrint(request()->all());
        if (!$data) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    public function updatePrint(UpdatePrint $request, $id)
    {
        $result = $this->deviceRepo->updatePrint(request()->all(), $id);
        if ($result === false) {
            return response()->json(['status' => 1], 404);
        }
        return response()->json(['status' => 0]);
    }

    public function destroyPrint($id)
    {
        $result = $this->deviceRepo->deletePrint($id);
        if ($result === false) {
            return response()->json(['status' => 1], 404);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * SetupAntenna
     */
    public function storeAntenna()
    {
        # code...
    }

    public function updateAntenna()
    {
        # code...
    }

    public function destroyAntenna()
    {
        # code...
    }
}
