<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SetupRepository;
use App\Http\Requests\Setup\CreateSuspend;
use App\Http\Requests\Setup\CreateException;
use App\Http\Requests\Setup\CreateUnfinished;

/**
 * 異常原因設定、除外原因設定、尾數原因設定 
 */

class SetupController extends Controller
{
    protected $setupRepo;

    public function __construct(SetupRepository $setupRepo)
    {
        $this->setupRepo = $setupRepo;
    }

    /**
     * 尾數原因設定
     */
    public function showNgPage()
    {
        $ngInfo = $this->setupRepo->getNgIndex();
        return view('setup.unfinishedPage', ['ngInfo' => $ngInfo]);
    }

    /**
     * 異常原因設定
     */
    public function showExceptionPage()
    {
        $exceptionInfo = $this->setupRepo->getExceptionIndex();
        return view('setup.exceptionPage', ['exceptionInfo' => $exceptionInfo]);
    }

    /**
     * 除外原因設定
     */
    public function showSuspendPage()
    {
        $suspendInfo = $this->setupRepo->getSuspendIndex();
        return view('setup.suspendPage', ['suspendInfo' => $suspendInfo]);
    }

    /**
     * 尾數原因設定 - 新增
     */
    public function storeUnfinished(CreateUnfinished $request)
    {
        $param = $request->only('nf_code', 'exception');
        $data = $this->setupRepo->createUnfinished($param);
        if (!$data) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 尾數原因設定 - 更新
     */
    public function updateUnfinished($id)
    {
        $param = request()->only('exception');
        $data = $this->setupRepo->updateUnfinished($param, $id);
        if ($data === 2) {
            return response()->json(['status' => 2]);
        }
        if ($data === 1) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 尾數原因設定 - 刪除
     */
    public function destroyUnfinished($id)
    {
        $result = $this->setupRepo->deleteUnfinished($id);
        if ($result === 2) {
            return response()->json(['status' => 2]);
        }
        if ($result === 1) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }
    
    /**
     * 異常原因設定 - 新增
     */
    public function storeException(CreateException $request)
    {
        $param = $request->only('ex_code', 'exception');
        $data = $this->setupRepo->createException($param);
        if (!$data) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 異常原因設定 - 更新
     */
    public function updateException($id)
    {
        $param = request()->only('exception');
        $data = $this->setupRepo->updateException($param, $id);
        if ($data === 2) {
            return response()->json(['status' => 2]);
        }
        if ($data === 1) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 異常原因設定 - 刪除
     */
    public function destroyException($id)
    {
        $result = $this->setupRepo->destroyException($id);
        if ($result === 2) {
            return response()->json(['status' => 2]);
        }
        if ($result === 1) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 除外原因設定 - 新增
     */
    public function storeSuspend(CreateSuspend $request)
    {
        $param = request()->only('sp_code', 'suspend');
        $data = $this->setupRepo->createSuspend($param);
        if (!$data) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 除外原因設定 - 更新
     */
    public function updateSuspend($id)
    {
        $param = request()->only('suspend');
        $data = $this->setupRepo->updateSuspend($param, $id);
        if ($data === 2) {
            return response()->json(['status' => 2]);
        }
        if ($data === 1) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }

    /**
     * 除外原因設定 - 刪除
     */
    public function destroySuspend($id)
    {
        $result = $this->setupRepo->deleteSuspend($id);
        if ($result === 2) {
            return response()->json(['status' => 2]);
        }
        if ($result === 1) {
            return response()->json(['status' => 1]);
        }
        return response()->json(['status' => 0]);
    }
}
