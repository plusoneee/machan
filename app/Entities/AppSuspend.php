<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class AppSuspend extends Model
{
    protected $fillable = [
    	'code', 'notification_id', 'message', 'title', 'content',
    	'infor_date', 'infor_time', 'name', 'role_name', 'mac_id',
    	'rfid_type', 'record_id', 'company_id', 'org_id', 'rfid_status',
    	'date', 'mo_id', 'item', 'qty', 'completion_date', 'so_id',
    	'customer_id', 'customer_name', 'mo_status', 'line_id',
    	'line_name', 'routing', 'person_id', 'role_id', 'tablet_id',
    	'sp_id', 'sp_code', 'sp_reason', 'suspend', 'reply',
    	'reply_date', 'reply_time'
    ];
}
