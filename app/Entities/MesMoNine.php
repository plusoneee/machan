<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoNine extends Model
{
    protected $fillable = [
        'dashboard_id', 'factory_id', 'line_id', 'mo_status', 'mo_id',
        'item', 'qty', 'so_id', 'customer_id', 'customer_name', 'group_id', 'group_qty',
        'prod_qty', 'fin_qty', 'ng_qty', 'start_datetime', 'finish_datetime', 'f_completion_time',
        'rest_time', 'about_qty',
    ];
}
