<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EndTemp extends Model
{
    protected $fillable = [
        'rfid_type', 'tag_id', 'mac_id', 'rfid_status', 'factory_id',
        'line_id', 'mo_status', 'mo_id', 'group_id', 'group_qty',
        'start_line', 'start_profile', 'start_antenna_id', 'start_datetime',
        'end_line', 'end_profile', 'end_antenna_id', 'end_datetime',
        'unfinish_qty', 'last', 'current_finish', 'ng', 'mo_finish', 
        'continuous', 'continuous_param', 'finish_insert', 'mix_order', 'cross_line', 'except',
        'real_rest_time', 'rest_mo_rest_time', 'acture_human', 'qty_hour', 'manufacture',
        'sum_real_tct', 'sum_real_tht', 'real_tht', 'real_ht',
    ];
}
