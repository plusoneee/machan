<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupLine extends Model
{
    protected $fillable = [
    	'line_id', 'line_name', 'company_id', 'org_id',
    	'routing', 'type', 'human', 'profile', 'status',
    	'note', 'first_condition', 'second_condition',
	];
	
	public function relatedAntenna()
	{
		return $this->hasMany('App\Entities\SetupAntenna', 'profile', 'profile');
	}
}
