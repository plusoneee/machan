<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupSuspend extends Model
{
    protected $fillable = [
    	'sp_code', 'suspend', 'note'
    ];
}