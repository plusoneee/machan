<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupReader extends Model
{
    protected $fillable = [
    	'reader_id', 'reader_name', 'mac_id', 'reader_ip',
    	'line_id', 'org_id', 'routing', 'profile', 'note'
    ];

    public function relatedLine()
    {
        return $this->hasOne('App\Entities\SetupLine', 'profile', 'profile');
    }
}