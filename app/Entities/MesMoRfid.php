<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoRfid extends Model
{
    protected $fillable = [
    	'mo_status', 'serial_no', 'cl_start_time', 'cl_end_time',
    	'mac_id', 'rfid_type', 'date', 'mo_id', 'item',
    	'qty', 'so_id', 'customer_id', 'customer_name',
    	'start_line', 'start_profile', 'start_antenna_id',
    	'start_datetime', 'end_line', 'end_profile',
    	'end_antenna_id', 'end_datetime', 'qc_pass',
    	'qc', 'finish', 'finish_time', 'ng', 'except_reason',
    	'ex_start_time', 'ex_end_time', 'suspend', 'code',
    	'notification_id', 'ntf_date_time', 'notification',
    	'rfid_status', 'dashboard_id', 'immediate_tct',
    	'immediate_ct', 'immediate_qty_per_hr',
    	'immediate_productivity', 'capacity_rate',
    	'color', 'immediate_equivalents', 'remark'
    ];
}