<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesQcPass extends Model
{
    protected $fillable = [
    	'qc_year', 'mac_id', 'rfid_type', 'record_id', 'company_id',
    	'org_id', 'rfid_status', 'record_date', 'erase_date',
    	'disable_date', 'enable', 'qc_pass', 'qc', 'print_date',
    	'print_time', 'force_print_date', 'force_print_time',
    	'reprint_date', 'reprint_time', 'printed', 'force_print',
    	'mo_id', 'item', 'qty', 'completion_date', 'so_id',
    	'customer_id', 'customer_name', 'mo_status', 'device_id',
    	'antenna_id', 'antenna_type', 'port_no', 'port_id',
    	'pc', 'fcc', 'crc', 'factory_id'

    ];
}