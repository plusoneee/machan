<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupException extends Model
{
    protected $fillable = [
    	'ex_code', 'exception', 'note'
    ];
}
