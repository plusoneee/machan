<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoHeaderEight extends Model
{
    protected $fillable = [
        'mo_status', 'mo_id', 'group_id', 'group_qty',
        'rest_time', 'accu_change_time',
        'acture_human', 'target_tht', 'target_ht',
        'sum_tct', 'sum_tht', 'first_pass_rate',
    ];
}
