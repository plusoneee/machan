<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SysLogin extends Model
{
    protected $fillable = [
    	'sys_id', 'name', 'type', 'company_id', 'org_id',
    	'group', 'short_name', 'active', 'tel', 'address',
    	'note'
    ];
}