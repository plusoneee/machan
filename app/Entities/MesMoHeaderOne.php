<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoHeaderOne extends Model
{
    protected $fillable = [
        'factory_id', 'line_id', 'mo_status', 'mo_id', 'group_id',
        'current_start_date', 'current_start_time', 'current_finish_date', 'current_finish_time',
        'prod_qty', 'fin_qty', 'ng_qty', 'current_rest_time', 'off_work_rest_time', 
        'cl_start_time', 'cl_end_time', 'change_time', 'tct',
    ];
}
