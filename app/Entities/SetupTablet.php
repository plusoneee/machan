<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupTablet extends Model
{
    protected $fillable = [
    	'tablet_id', 'mac_id', 'role', 'ip', 'line_id',
    	'org_id', 'routing', 'profile', 'antenna_id',
    	'note'
    ];

    public function relatedAntenna()
    {
        return $this->hasOne('App\Entities\SetupAntenna', 'device_id', 'tablet_id');
    }

    public function relatedLine()
    {
    	return $this->hasOne('App\Entities\SetupLine', 'profile', 'profile');
    }

    public function relatedReader()
    {
        return $this->belongsTo('App\Entities\SetupReader', 'profile', 'profile');
    }
}