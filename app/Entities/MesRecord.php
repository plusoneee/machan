<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesRecord extends Model
{
    protected $fillable = [
    	'record_date', 'mo_id', 'item', 'mo_qty', 'mac_id',
    	'rfid_type', 'rfid_status', 'company_id',
    	'factory_id', 'so_id', 'customer_id', 'customer_name',
		'record_id', 'complete_date',
		'group_id', 'group_qty', 'group_line',
    ];
}