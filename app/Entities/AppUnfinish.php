<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class AppUnfinish extends Model
{
    protected $fillable = [
    	'code_nf', 'red_mac_id', 'rfid_type', 'nf_type',
    	'mo_qty', 'person_id', 'name', 'tablet_id',
		'ex_type', 'except_reason', 'reply',
    	'reply_date', 'reply_time',
    	'ex_start_time', 'ex_end_time',
    	'nf_status', 'mac_id', 'record_id',
    	'date', 'mo_id', 'item', 'so_id', 'customer_id',
    	'customer_name', 'start_line', 'start_profile',
    	'start_antenna_id', 'start_date', 'end_line',
    	'end_profile', 'end_antenna_id', 'end_date',
    	'end_time', 'qc_pass', 'qc', 'finish',
    	'finish_time', 'ng', 'suspend',
    	'mo_status', 'code', 'notification_id',
    	'ntf_data_time', 'notification',
    	'rfid_status', 'dashboard_id', 'remark'
    ];
}