<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SyncUser extends Model
{
    protected $fillable = [
    	'account', 'employee_id', 'name', 'active', 'note'
    ];
}
