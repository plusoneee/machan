<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupNotification extends Model
{
    protected $fillable = [
    	'code', 'notification_id', 'message', 'title',
    	'content', 'method', 'timing', 'cycle',
    	'assign_type', 'name', 'role_name', 'line_id',
    	'sender', 'receiver', 'ref', 'next_action',
    	'ntf_table', 'note'
    ];
}