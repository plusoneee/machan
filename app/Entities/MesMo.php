<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMo extends Model
{
    protected $fillable = [
    	'record_date', 'mo_id', 'serial_no', 'accumulator_start',
    	'accumulator_fin', 'change_date', 'change_time',
    	'change_line_id', 'change_line_name', 'change_profile',
    	'mo_status', 'item', 'qty', 'completion_date',
    	'so_id', 'customer_id', 'customer_name', 'date',
    	'date', 'line_id', 'line_name', 'profile',
    	'start_time', 'finish_date', 'finish_time',
    	'finish', 'first_start_time', 'first_finish_date',
    	'first_finish_time', 'rest_time', 'prod_qty', 'fin_qty', 'nf_qty',
    	'change_lead_time', 'cl_start_time', 'cl_end_time',
    	'nex_id', 'suspend_time', 'first_pass_rate',
    	'acture_human', 'tht', 'ht', 'tct', 'ct',
    	'qty_per_hr', 'productivity', 'reach_rate',
    	'f_completion_time', 'target_qty', 'suspend_rate',
    	'std_change_lead', 'equivalents', 'immediate_tct',
    	'immediate_ct', 'immediate_qty_per_hr',
    	'immediate_productivity', 'capacity_rate',
    	'color', 'immediate_equivalents', 'code',
    	'notification_id', 'ntf_date_time',
		'notification', 'remark', 'record_id',
		'group_id', 'group_qty', 'group_line',
    ];

    public function relatedEbMo()
    {
        return $this->hasOne('App\Entities\EbMo', 'mo_id', 'mo_id');
    }

    public function relatedRfid()
    {
        return $this->hasMany('App\Entities\MesRfid', 'mo_id', 'mo_id');
    }

    public function relatedLine()
    {
        return $this->hasOne('App\Entities\SetupLine', 'profile', 'profile');
    }
}