<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoHeaderTwo extends Model
{
    protected $fillable = [
        'factory_id', 'line_id', 'inserted_mo_id', 'mo_id', 'group_id',
        'delta_t', 'insert_start_date', 'insert_start_time', 'insert_finish_date', 'insert_finish_time',
        'accu_insert_start', 'accu_insert_finish', 'cl_start_time', 'cl_end_time', 'insert_change_time', 'insert_delta_tct',
    ];
}
