<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupNonFinished extends Model
{
    protected $fillable = [
    	'nf_code', 'exception', 'note'
    ];
}