<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class StartTemp extends Model
{
    protected $fillable = [
        'rfid_type', 'tag_id', 'mac_id', 'rfid_status', 'factory_id',
        'line_id', 'mo_status', 'mo_id', 'group_id', 'group_qty',
        'start_line', 'start_profile', 'start_antenna_id',
        'start_datetime', 'first', 'continuous', 'continuous_param',
        'judge_insert', 'cross_line', 'sep_order', 'except',
        'day_rank', 'cl_start_time', 'cl_end_time', 'change_time', 'except_notify',
        's_change_time', 'insert_change_time', 'sep_change_time',
    ];
}
