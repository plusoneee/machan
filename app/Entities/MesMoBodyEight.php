<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MesMoBodyEight extends Model
{
    protected $fillable = [
        'type', 'factory_id', 'line_id', 'mo_status', 'mo_id', 'group_id',
        'prod_qty', 'fin_qty', 'accu_cross_fin_qty',
        'rest_time', 'work_off_rest_time', 'mix_rest_time', 'sep_rest_time', 'accu_sep_rest_time',
        'cross_day_rest_time', 'accu_cross_day_rest', 'accu_cross_fac_rest',
        'change_time', 'accu_change_time', 'insert_change_time', 'accu_insert_change_time',
        'cross_change_time', 'accu_cross_change_time',
        'delta_t', 'sum_tct', 'insert_delta_tct', 'mix_sum_tct', 'ng_delta_tct', 'sep_sum_tct', 'non_sep_delta_tct',
        'con_day_sum_tct', 'non_con_delta_tct', 'cross_fac_sum_tct',
    ];
}
