@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">使用者註冊</div>

                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">姓名</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="account" class="col-md-4 control-label">帳號</label>

                            <div class="col-md-6">
                                <input id="account" type="text" class="form-control" name="account" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">密碼</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">確認密碼</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="register()">
                                    註冊
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('subPageJs')
<script>
    const register = () => {
        event.preventDefault()
        var name = document.getElementById('name').value
        var account = document.getElementById('account').value
        var password =  document.getElementById('password').value
        var cofirmPassword =  document.getElementById('password-confirm').value

        if (password === cofirmPassword) {
            $.ajax({
                url: '/register',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    name: name,
                    account: account,
                    password: password
                },
                success: data => {
                    window.location.href = '/'
                },
                statusCode: {
                    422: result => {
                        if (result.responseJSON.account) {
                            alert('此帳號已被註冊')
                        }
                    }
                }
            })
        } else {
            alert('密碼必須與確認密碼相同')   
        }
    }
</script>
@endsection
