@extends('layouts.app')

@section('subPageCss')
 <style>
	table {
  		table-layout: fixed;
  		word-wrap: break-word;
        font-size: 14pt;
	}
	.mainBtn {
  		font-weight: bold;
  		color: black;
	}
	.searchInput {
  		width: 50px;
	}
	.modal-dialog {
  		margin-top: 20vh;
	}
 </style>
@endsection

@section('content')
<div class="container-fluid" style="width:100%;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size: 30px; font-weight: bold;">
                    製令RFID報工數據
                    {{--  <button type="button" class="btn btn-default mainBtn" data-toggle="modal" data-target=".bs-example-modal-sm">查詢</button>  --}}
                    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="form-group">
                                <label for="exampleInputEmail2" style="color: white;">依製令查詢</label>
                                <input type="text" class="form-control" id="moStart">
                                <p style="color: white;">至</p>
                                <input type="text" class="form-control" id="moEnd">
								<button type="submit" class="btn btn-default mainBtn" id="moSubmit">Submit</button><br />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
					<div class="form-group row">
						<label class="col-sm-1 col-form-label">製令單號</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="mo_id" />
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-1 col-form-label">來源訂單號</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="so_id" />
						</div>
					</div>
                    <div class="form-group row">
						<label class="col-sm-1 col-form-label">報工日期</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="start_date" />
						</div>
					</div>
                    <div class="form-group row">
						<label class="col-sm-1 col-form-label">完工日期</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="fin_date" />
						</div>
					</div>
                    <div class="form-group row">
						<label class="col-sm-1 col-form-label">製令狀態</label>
						<div class="col-sm-3">
							<select class="form-control" id="mo_status">
                                <option value="">請選擇製令狀態</option>
								<option value="0">0：已錄入</option>
								<option value="1">1：生產中</option>
								<option value="2">2：跨天未完工</option>
                                <option value="3">3：有尾數完工</option>
                                <option value="4">4：跨多天未完工</option>
                                <option value="5">5：完工結案</option>
                                <option value="6">6：強制結案</option>
                                <option value="7">7：取消</option>
							</select>
						</div>
					</div>
                    <div class="form-group row">
						<label class="col-sm-1 col-form-label">設備識別碼</label>
						<div class="col-sm-3">
							<select class="form-control" id="profile">
                                <option value="">請選擇線別</option>
								<option value="01100004A001">一群裝配A線</option>
								<option value="01100004B002">一群裝配B線</option>
								<option value="01100004C003">一群裝配C線</option>
                                <option value="01100204A004">二群裝配A線</option>
								<option value="01100204B005">二群裝配B線</option>
								<option value="01100204C006">二群裝配C線</option>
							</select>
						</div>
						<input type="submit" class="btn btn-danger" value="搜尋" id="submit" />
					</div>
				</div>

                <div class="panel-body">
                  	<table id="table" class="table table-bordered">
                      	<tr>
                            <td class="info">序號</td>
                            <td class="info">製令單號</td>
                      		<td class="info">報工日期</td>
                            <td class="info">製令狀態</td>
                            <td class="info">報工明細</td>
                            <td class="info">來源訂單號</td>
                            <td class="info">客戶名稱</td>
                            <td class="info">物料代碼</td>
                            <td class="info">訂單數量</td>
                            <td class="info">生產數量</td>
                            <td class="info">尾數</td>
                            <td class="info">換模開始</td>
                            <td class="info">換模結束</td>
                      	</tr>
                  	</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('subPageJs')
<script type="text/javascript">
    var datat = $('<tbody id="tbody"/>');
	
	$('#submit').click(() => {
		var moId = document.getElementById('mo_id').value
		var soId = document.getElementById('so_id').value
		var date = document.getElementById('start_date').value
		var finDate = document.getElementById('fin_date').value
        var moStatus = document.getElementById('mo_status').value
        var profile = document.getElementById('profile').value

		$.ajax({
			'url': '/mes/mo/rfid/data',
			'type': 'POST',
			'dataType': 'JSON',
			'data': {
				mo_id: moId,
				so_id: soId,
				date: date,
                fin_date: finDate,
                mo_status: moStatus,
				profile: profile
			},
			success: (data) => {
				showData(data)
			}
		})
	})
	
	showData = (data) => {
        $('.active').remove();
        data.map((m, key) => {
			datat.append('<tr role="row">'
                +'<td class="active">'+m.id+'</td>'
                +'<td class="active">'+m.mo_id+'</td>'
                +'<td class="active">'+m.date+'</td>'
                +'<td class="active">'+m.mo_status+'</td>'
                +'<td class="active">'+'<a href="" role="button" data-toggle="modal" data-target=".bs-example-modal-sm">查看</a>'+'</td>'
                +'<td class="active">'+m.so_id+'</td>'
                +'<td class="active">'+m.customer_name+'</td>'
                +'<td class="active">'+m.item+'</td>'
                +'<td class="active">'+m.qty+'</td>'
                +'<td class="active">'+m.accumulator_fin+'</td>'
                +'<td class="active">'+m.nf_qty+'</td>'
                +'<td class="active">'+m.cl_start_time+'</td>'
                +'<td class="active">'+m.cl_end_time+'</td>'
            );
        })

        $('#table').append(datat);
    }

</script>
@endsection



