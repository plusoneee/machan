#!/bin/bash
cd /var/www/html/machan/

chmod -R 777 .

php artisan key:generate
service redis-server restart
php artisan channel:AdvancePrintSubscribe &
php artisan channel:EraseSubscribe &
php artisan channel:MappingSubscribe &
php artisan channel:PassEndSubscribe &
php artisan channel:PassStartSubscribe &
php artisan channel:PrintQcPassSubscribe &
php artisan channel:RedisSubscribe &

/usr/sbin/apache2ctl -D FOREGROUND
